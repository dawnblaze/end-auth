﻿using System;

namespace Endor.Auth.Models.Entities
{
    public partial class UserInvite
    {
        public Guid ID { get; set; }
        public short BID { get; set; }
        public bool IsEmployee { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime ExpirationDT { get; set; }

        public virtual BusinessData Business { get; set; }
    }
}
