﻿using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Entities
{
    public partial class DatabaseData
    {
        public DatabaseData()
        {
            BusinessDataBusinessDatabase = new HashSet<BusinessData>();
            BusinessDataLoggingDatabase = new HashSet<BusinessData>();
            BusinessDataReportDatabase = new HashSet<BusinessData>();
            BusinessDataSystemDatabase = new HashSet<BusinessData>();
        }

        public short ID { get; set; }
        public string Server { get; set; }
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
        public string BackupPath { get; set; }
        public DateTime? LastBackupDt { get; set; }
        public bool IsSystemDatabase { get; set; }
        public bool IsReportDatabase { get; set; }
        public bool IsLoggingDatabase { get; set; }
        public bool IsCustomerDatabase { get; set; }

        public virtual ICollection<BusinessData> BusinessDataBusinessDatabase { get; set; }
        public virtual ICollection<BusinessData> BusinessDataLoggingDatabase { get; set; }
        public virtual ICollection<BusinessData> BusinessDataReportDatabase { get; set; }
        public virtual ICollection<BusinessData> BusinessDataSystemDatabase { get; set; }
    }
}
