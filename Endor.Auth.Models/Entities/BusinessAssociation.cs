﻿using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Entities
{
    public partial class BusinessAssociation
    {
        public BusinessAssociation()
        {
            BusinessData = new HashSet<BusinessData>();
        }

        public byte ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<BusinessData> BusinessData { get; set; }
    }
}
