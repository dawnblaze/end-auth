﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Auth.Models.Entities
{
    public class RefreshToken
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public short BID { get; set; }
        public string Token { get; set; }
        public Guid Nonce { get; set; }
        public DateTime ExpiresDT { get; set; }
        public DateTime IssuedDT { get; set; }
        public DateTime RevokedDT { get; set; }
        public DateTime LastUsedDT { get; set; }
        public string ClientID { get; set; }

        public virtual BusinessData Business { get; set; }
        public virtual UserData User { get; set; }
    }
}
