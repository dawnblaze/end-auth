﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Endor.Tenant;

namespace Endor.Auth.Models.Entities
{
    public partial class AuthContext : DbContext
    {
        public virtual DbSet<BusinessAssociation> BusinessAssociation { get; set; }
        public virtual DbSet<BusinessCustomUrl> BusinessCustomUrl { get; set; }
        public virtual DbSet<BusinessData> BusinessData { get; set; }
        public virtual DbSet<ServerData> ServerData { get; set; }
        public virtual DbSet<ServerLink> ServerLink { get; set; }
        public virtual DbSet<UserBusinessLink> UserBusinessLink { get; set; }
        public virtual DbSet<ClientData> ClientData { get; set; }
        public virtual DbSet<ClientDevice> ClientDevice { get; set; }
        public virtual DbSet<DatabaseData> DatabaseData { get; set; }
        public virtual DbSet<EnumAuthenticationType> EnumAuthenticationType { get; set; }
        public virtual DbSet<EnumServerType> EnumServerType { get; set; }
        public virtual DbSet<UserData> UserData { get; set; }
        public virtual DbSet<UserInvite> UserInvite { get; set; }
        public virtual DbSet<RefreshToken> RefreshToken { get; set; }
        public virtual DbSet<UserCred> UserCred { get; set; }

        public AuthContext(DbContextOptions<AuthContext> options) : base(options)
        {
            this.Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BusinessAssociation>(entity =>
            {
                entity.ToTable("Business.Association");

                entity.Property(e => e.ID)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<BusinessCustomUrl>(entity =>
            {
                entity.ToTable("Business.CustomURL");

                entity.HasIndex(e => new { e.BID, e.CustomUrl, e.IsActive })
                    .HasName("IX_Business.CustomURL_BID");

                entity.HasIndex(e => new { e.CustomUrl, e.BID, e.IsActive })
                    .HasName("IX_Business.CustomURL_URL");

                entity.Property(e => e.ID).HasColumnName("ID");

                entity.Property(e => e.BID).HasColumnName("BID");

                entity.Property(e => e.CreatedDT)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("getutcdate()");

                entity.Property(e => e.CustomUrl)
                    .IsRequired()
                    .HasColumnName("CustomURL")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.HasSsl)
                    .HasColumnName("HasSSL")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.InactiveDT)
                    .HasColumnName("InactiveDT")
                    .HasColumnType("datetime2(0)");

                entity.Property(e => e.IsActive)
                    .HasComputedColumnSql("isnull(case when [InactiveDT] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.MetaData).HasColumnType("xml");

                entity.Property(e => e.SslexpirationDT).HasColumnName("SSLExpirationDT");

                entity.HasOne(d => d.B)
                    .WithMany(p => p.BusinessCustomUrl)
                    .HasForeignKey(d => d.BID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Business.CustomURL_Business.Data");
            });

            modelBuilder.Entity<BusinessData>(entity =>
            {
                entity.HasKey(e => e.BID)
                    .HasName("PK_Business");

                entity.ToTable("Business.Data");

                entity.HasIndex(e => new { e.BusinessAppName, e.IsActive })
                    .HasName("IX_Business.Data_AppName");

                entity.HasIndex(e => new { e.BusinessDatabaseID, e.IsActive })
                    .HasName("IX_Business.Data_DatabaseID");

                entity.HasIndex(e => new { e.CompanyName, e.IsActive })
                    .HasName("IX_Business.Data_CompanyName");

                entity.Property(e => e.BID)
                    .HasColumnName("BID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AssociationID)
                    .HasColumnName("AssociationID")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.BusinessAppName)
                    .IsRequired()
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.BusinessDatabaseID).HasColumnName("BusinessDatabaseID");
                
                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CreatedDT)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("getutcdate()");

                entity.Property(e => e.InactiveDT)
                    .HasColumnName("InactiveDT")
                    .HasColumnType("datetime2(0)");

                entity.Property(e => e.IsActive)
                    .HasComputedColumnSql("isnull(case when [InactiveDT] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.LoggingDatabaseID).HasColumnName("LoggingDatabaseID");

                entity.Property(e => e.NodeID).HasColumnName("NodeID");

                entity.Property(e => e.ReportDatabaseID).HasColumnName("ReportDatabaseID");

                entity.Property(e => e.StorageConnectionString)
                    .IsRequired()
                    .HasColumnName("StorageConnectionString")
                    .HasColumnType("varchar(512)");

                entity.Property(e => e.IndexStorageConnectionString)
                    .IsRequired()
                    .HasColumnName("IndexStorageConnectionString")
                    .HasColumnType("varchar(512)");

                entity.Property(e => e.WebServerDomain)
                    .IsRequired()
                    .HasColumnName("WebServerDomain")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.SystemDatabaseID).HasColumnName("SystemDatabaseID");

                entity.HasOne(d => d.Association)
                    .WithMany(p => p.BusinessData)
                    .HasForeignKey(d => d.AssociationID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Business.Data_Business.Association");

                entity.HasOne(d => d.BusinessDatabase)
                    .WithMany(p => p.BusinessDataBusinessDatabase)
                    .HasForeignKey(d => d.BusinessDatabaseID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Business.Data_Database.Data1");

                entity.HasOne(d => d.LoggingDatabase)
                    .WithMany(p => p.BusinessDataLoggingDatabase)
                    .HasForeignKey(d => d.LoggingDatabaseID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Business.Data_Database.Data4");

                entity.HasOne(d => d.ReportDatabase)
                    .WithMany(p => p.BusinessDataReportDatabase)
                    .HasForeignKey(d => d.ReportDatabaseID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Business.Data_Database.Data3");

                entity.HasOne(d => d.SystemDatabase)
                    .WithMany(p => p.BusinessDataSystemDatabase)
                    .HasForeignKey(d => d.SystemDatabaseID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Business.Data_Database.Data2");
            });

            modelBuilder.Entity<ClientData>(entity =>
            {
                entity.ToTable("Client.Data");

                entity.HasIndex(e => e.BID)
                    .HasName("IX_Client.Connection_BID");

                entity.HasIndex(e => e.ClientID)
                    .HasName("IX_Client.Connection_Client");

                entity.Property(e => e.ID)
                    .HasColumnName("ID")
                    .UseIdentityColumn();

                entity.Property(e => e.BID).HasColumnName("BID");

                entity.Property(e => e.ClientID)
                    .IsRequired()
                    .HasColumnName("ClientID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ConnectDT)
                    .HasColumnName("ConnectDT")
                    .HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("getutcdate()");

                entity.Property(e => e.DeviceID).HasColumnName("DeviceID");

                entity.Property(e => e.InactiveMinutes)
                    .HasComputedColumnSql("datediff(minute,[LastActivityDT],getutcdate())")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.LastActivityDT)
                    .HasColumnName("LastActivityDT")
                    .HasColumnType("datetime2(2)");

                entity.Property(e => e.UserID).HasColumnName("UserID");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.ClientData)
                    .HasForeignKey(d => d.BID)
                    .HasConstraintName("FK_Client.Connection_Business.Data");

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.ClientData)
                    .HasForeignKey(d => d.DeviceID)
                    .HasConstraintName("FK_Client.Connection_Client.Device");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ClientData)
                    .HasForeignKey(d => d.UserID)
                    .HasConstraintName("FK_Client.Connection_User.Data");
            });

            modelBuilder.Entity<ClientDevice>(entity =>
            {
                entity.ToTable("Client.Device");

                entity.Property(e => e.ID)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.RegEx).HasColumnType("varchar(max)");
            });

            modelBuilder.Entity<DatabaseData>(entity =>
            {
                entity.ToTable("Database.Data");

                entity.Property(e => e.ID)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BackupPath).HasColumnType("varchar(max)");

                entity.Property(e => e.ConnectionString).HasColumnType("varchar(max)");

                entity.Property(e => e.DatabaseName).HasColumnType("varchar(255)");

                entity.Property(e => e.IsCustomerDatabase)
                    .HasComputedColumnSql("~(([IsSystemDatabase]|[IsReportDatabase])|[IsLoggingDatabase])")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.LastBackupDt)
                    .HasColumnName("LastBackupDT")
                    .HasColumnType("datetime2(2)");

                entity.Property(e => e.Server).HasColumnType("varchar(1024)");
            });

            modelBuilder.Entity<ServerData>(entity =>
            {
                entity.ToTable("Server.Data");

                entity.HasKey(e => new {e.ID });

                entity.Property(e => e.Name).HasColumnType("varchar(255)");
                entity.Property(e => e.URL).HasColumnType("varchar(max)");

                entity.Property(e => e.ServerType).HasColumnName("ServerType").HasColumnType("tinyint");

                entity.HasOne(d => d.EnumServerType)
                            .WithMany(p => p.ServerData)
                            .HasForeignKey(d => d.ServerType)
                            .OnDelete(DeleteBehavior.Restrict)
                            .HasConstraintName("FK_Server.Data_enum.Server.Type");
            });

            modelBuilder.Entity<ServerLink>(entity =>
            {
                entity.ToTable("Business.ServerLink");

                entity.HasKey(e => new { e.BID, e.ServerID });

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.ServerLinks)
                    .HasForeignKey(d => d.BID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Business.ServerLink_Business.Data");

                entity.HasOne(d => d.Server)
                            .WithMany(p => p.ServerLinks)
                            .HasForeignKey(d => d.ServerID)
                            .OnDelete(DeleteBehavior.Restrict)
                            .HasConstraintName("FK_Business.ServerLink_Server.Data");
            });

            modelBuilder.Entity<EnumServerType>(entity =>
            {
                entity.ToTable("enum.Server.Type");

                entity.Property(e => e.ID)
                    .HasColumnName("ID")
                    .HasColumnType("tinyint")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<EnumAuthenticationType>(entity =>
            {
                entity.ToTable("enum.Authentication.Type");

                entity.Property(e => e.ID)
                    .HasColumnName("ID")
                    .HasColumnType("tinyint")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<UserBusinessLink>(entity =>
            {
                entity.ToTable("User.BusinessLink");

                entity.HasKey(e => new { e.BID, e.UserID });
                entity.Property(e => e.BID)
                    .HasColumnName("BID")
                    .ValueGeneratedNever();

                entity.Property(e => e.UserID)
                    .HasColumnName("UserID")
                    .ValueGeneratedNever();

                entity.Property(e => e.LastLoggedInDT)
                    .IsRequired(false)
                    .HasColumnType("datetime2(2)");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.UserBusinessLinks)
                    .HasForeignKey(d => d.BID)
                    .HasConstraintName("FK_User.BusinessLink_Business.Data");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.BusinessLinks)
                    .HasForeignKey(d => d.UserID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("PK_User.BusinessLink");
            });

            modelBuilder.Entity<UserData>(entity =>
            {
                entity.ToTable("User.Data");

                entity.HasIndex(e => e.EmailAddress)
                    .HasName("IX_User.Data_EmailAddess");

                entity.HasIndex(e => e.UserName)
                    .HasName("IX_User.Data_UserName");

                entity.Property(e => e.ID).HasColumnName("ID");

                entity.Property(e => e.DisplayName).HasColumnType("varchar(255)");

                entity.Property(e => e.EmailAddress).HasColumnType("varchar(255)");
                entity.Property(e => e.PhoneNumber).HasColumnType("varchar(255)");

                entity.Property(e => e.LastPwChangeDT)
                    .HasColumnName("LastPWChangeDT")
                    .HasColumnType("datetime2(0)");

                entity.Property(e => e.MustResetPw)
                    .HasColumnName("MustResetPW")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.PWResetToken)
                    .IsRequired(false)
                    .HasColumnType("uniqueidentifier");

                entity.Property(e => e.PWResetTokenExpirationDT)
                    .HasColumnName("PWResetTokenExpirationDT")
                    .HasColumnType("datetime2(0)");
            });

            modelBuilder.Entity<UserCred>(entity =>
            {
                entity.ToTable("User.Cred");

                entity.HasKey(e => new { e.UserID, e.AuthenticationType });

                entity.Property(e => e.UserID).HasColumnName("UserID");
                entity.Property(e => e.AuthenticationType).HasColumnName("AuthenticationType").HasColumnType("tinyint");
                entity.Property(e => e.AuthenticationHash).HasColumnName("AuthenticationHash");
                entity.Property(e => e.LastUsedDT).HasColumnName("LastUsedDT");

                entity.HasOne(d => d.User)
                            .WithMany(p => p.UserCreds)
                            .HasForeignKey(d => d.UserID)
                            .OnDelete(DeleteBehavior.Restrict)
                            .HasConstraintName("FK_User.Cred_User.Data");

                entity.HasOne(d => d.EnumAuthenticationType)
                            .WithMany(p => p.UserCreds)
                            .HasForeignKey(d => d.AuthenticationType)
                            .OnDelete(DeleteBehavior.Restrict)
                            .HasConstraintName("FK_User.Cred_enum.Authentication.Type");
            });

            modelBuilder.Entity<UserInvite>(entity =>
            {
                entity.ToTable("User.Invite");

                entity.Property(e => e.ID).HasColumnName("ID");
                entity.Property(e => e.BID).HasColumnName("BID");
                entity.Property(e => e.IsEmployee).HasColumnName("IsEmployee");
                entity.Property(e => e.EmailAddress).HasColumnName("EmailAddress");
                entity.Property(e => e.PhoneNumber).HasColumnName("PhoneNumber");
                entity.Property(e => e.ExpirationDT).HasColumnName("ExpirationDT");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.UserInvites)
                    .HasForeignKey(d => d.BID)
                    .HasConstraintName("FK_User.Invite_Business.Data");
            });

            modelBuilder.Entity<UserCred>(entity =>
            {
                entity.ToTable("User.Cred");

                entity.Property(e => e.UserID).HasColumnName("UserID");
                entity.Property(e => e.AuthenticationType).HasColumnName("AuthenticationType");
                entity.Property(e => e.AuthenticationHash).HasColumnName("AuthenticationHash");
                entity.Property(e => e.LastUsedDT).HasColumnName("LastUsedDT");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserCreds)
                    .HasForeignKey(d => d.UserID)
                    .HasConstraintName("FK_User.Cred_User.Data");
            });

            modelBuilder.Entity<RefreshToken>(entity =>
            {
                entity.ToTable("User.RefreshToken");
                entity.HasIndex(e => e.Token)
                    .HasName("IX_User.RefreshToken_Token");
                entity.HasIndex(e => e.Nonce)
                    .HasName("IX_User.RefreshToken_Nonce");
                entity.HasIndex(e => new { e.BID, e.UserID })
                    .HasName("IX_User.RefreshToken_BID_UserID");

                entity.Property(e => e.ID).HasColumnName("ID");
                entity.Property(e => e.BID).HasColumnName("BID").HasColumnType("smallint");
                entity.Property(e => e.Token).HasColumnName("Token").HasColumnType("varchar(255)");
                entity.Property(e => e.Nonce).IsRequired().HasColumnType("uniqueidentifier").HasDefaultValueSql("NEWID()");
                entity.Property(e => e.ExpiresDT).IsRequired().HasColumnName("ExpiresDT").HasColumnType("datetime2(2)").HasDefaultValueSql("GETUTCDATE()");
                entity.Property(e => e.IssuedDT).IsRequired().HasColumnName("IssuedDT").HasColumnType("datetime2(2)").HasDefaultValueSql("GETUTCDATE() + (32)");
                entity.Property(e => e.RevokedDT).HasColumnName("RevokedDT").HasColumnType("datetime2(2)");
                entity.Property(e => e.LastUsedDT).HasColumnName("LastUsedDT").HasColumnType("datetime2(2)");
                entity.Property(e => e.ClientID).HasColumnName("ClientID").HasColumnType("varchar(1024)");
                entity.HasOne(d => d.User)
                            .WithMany(p => p.RefreshTokens)
                            .HasForeignKey(d => d.UserID)
                            .OnDelete(DeleteBehavior.Restrict)
                            .HasConstraintName("FK_User.RefreshToken_User.Data");
                entity.HasOne(d => d.Business)
                            .WithMany(p => p.RefreshTokens)
                            .HasForeignKey(d => d.BID)
                            .OnDelete(DeleteBehavior.Restrict)
                            .HasConstraintName("FK_User.RefreshToken_Business.Data");
            });
        }
    }
}