﻿using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Entities
{
    public partial class ClientDevice
    {
        public ClientDevice()
        {
            ClientData = new HashSet<ClientData>();
        }

        public short ID { get; set; }
        public string Name { get; set; }
        public string RegEx { get; set; }

        public virtual ICollection<ClientData> ClientData { get; set; }
    }
}
