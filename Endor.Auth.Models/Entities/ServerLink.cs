﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Auth.Models.Entities
{
    public class ServerLink
    {
        public short BID { get; set; }
        public short ServerID { get; set; }

        public virtual BusinessData Business { get; set; }
        public virtual ServerData Server { get; set; }

    }
}
