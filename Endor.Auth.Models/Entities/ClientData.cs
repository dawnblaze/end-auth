﻿using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Entities
{
    public partial class ClientData
    {
        public long ID { get; set; }
        public string ClientID { get; set; }
        public short? DeviceID { get; set; }
        public short? BID { get; set; }
        public int? UserID { get; set; }
        public DateTime ConnectDT { get; set; }
        public DateTime LastActivityDT { get; set; }
        public int? InactiveMinutes { get; set; }

        public virtual BusinessData Business { get; set; }
        public virtual ClientDevice Device { get; set; }
        public virtual UserData User { get; set; }
    }
}
