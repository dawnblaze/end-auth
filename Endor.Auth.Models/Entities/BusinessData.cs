﻿using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Entities
{
    public partial class BusinessData
    {
        public BusinessData()
        {
            BusinessCustomUrl = new HashSet<BusinessCustomUrl>();
            ClientData = new HashSet<ClientData>();
            RefreshTokens = new HashSet<RefreshToken>();
            UserBusinessLinks = new HashSet<UserBusinessLink>();
        }

        public short BID { get; set; }
        public string BusinessAppName { get; set; }
        public bool IsActive { get; set; }
        public string CompanyName { get; set; }
        public byte AssociationID { get; set; }
        public DateTime CreatedDT { get; set; }
        public DateTime? InactiveDT { get; set; }
        public short BusinessDatabaseID { get; set; }
        public short SystemDatabaseID { get; set; }
        public short ReportDatabaseID { get; set; }
        public short LoggingDatabaseID { get; set; }

        public string StorageConnectionString { get; set; }
        public string IndexStorageConnectionString { get; set; }
        public string WebServerDomain { get; set; }

        public byte NodeID { get; set; }

        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }
        public virtual ICollection<BusinessCustomUrl> BusinessCustomUrl { get; set; }
        public virtual ICollection<ClientData> ClientData { get; set; }
        public virtual ICollection<UserBusinessLink> UserBusinessLinks { get; set; }
        public virtual ICollection<UserInvite> UserInvites { get; set; }
        public virtual ICollection<ServerLink> ServerLinks { get; set; }
        public virtual BusinessAssociation Association { get; set; }
        public virtual DatabaseData BusinessDatabase { get; set; }
        public virtual DatabaseData LoggingDatabase { get; set; }
        public virtual DatabaseData ReportDatabase { get; set; }
        public virtual DatabaseData SystemDatabase { get; set; }

    }
}
