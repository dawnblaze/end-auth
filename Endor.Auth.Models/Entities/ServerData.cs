﻿using Endor.Tenant;
using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Entities
{
    public partial class ServerData : BaseServerData
    {
        public ServerData()
        {
           
        }
        public virtual ICollection<ServerLink> ServerLinks { get; set; }
        public virtual EnumServerType EnumServerType { get; set; }
    }
}
