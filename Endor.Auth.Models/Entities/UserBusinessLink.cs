﻿using System;

namespace Endor.Auth.Models.Entities
{
    public partial class UserBusinessLink
    {
        public short BID { get; set; }
        public int UserID { get; set; }
        public DateTime? LastLoggedInDT { get; set; }

        public virtual BusinessData Business { get; set; }
        public virtual UserData User { get; set; }
    }
}
