﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Auth.Models.Entities
{
    public static class AuthContextExtensions
    {
        /// <summary>
        /// Gets a BusinessData object based on the businessAlias
        /// </summary>
        /// <param name="BID">Business ID</param>
        /// <returns></returns>
        public static async Task<BusinessData> GetBusinessByAliasAsync(this AuthContext ctx, string businessAlias)
        {
            return await ctx.BusinessData
                .Include(b => b.ServerLinks)
                .ThenInclude(sl => sl.Server)
                .FirstOrDefaultAsync(t => String.Equals(t.BusinessAppName, businessAlias));
        }

        public static async Task<BusinessData> GetBusinessByBIDAsync(this AuthContext ctx, short bid)
        {
            return await ctx.BusinessData
                .Include(b => b.ServerLinks)
                .ThenInclude(sl => sl.Server)
                .FirstOrDefaultAsync(t => t.BID == bid);
        }

        public static async Task<List<BusinessData>> GetAllBusinessesAsync(this AuthContext ctx)
        {
            return await Task.FromResult(GetAllBusinesses(ctx));
        }

        public static List<BusinessData> GetAllBusinesses(this AuthContext ctx)
        {
            return ctx.BusinessData
                .Include(b => b.ServerLinks)
                .ThenInclude(sl => sl.Server)
                .ToList();
        }

    }
}
