﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Endor.Tenant;

namespace Endor.Auth.Models.Entities
{
    public partial class EnumServerType
    {
        [Column("ID", TypeName = "tinyint")]
        public ServerType ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ServerData> ServerData { get; set; }
    }
}
