﻿using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Entities
{
    public partial class UserData
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string DisplayName { get; set; }
        public DateTime? LastPwChangeDT { get; set; }
        public bool MustResetPw { get; set; }
        public Guid? PWResetToken { get; set; }
        public DateTime? PWResetTokenExpirationDT { get; set; }

        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }
        public virtual ICollection<ClientData> ClientData { get; set; }
        public virtual ICollection<UserBusinessLink> BusinessLinks { get; set; }
        public virtual ICollection<UserCred> UserCreds { get; set; }
    }
}
