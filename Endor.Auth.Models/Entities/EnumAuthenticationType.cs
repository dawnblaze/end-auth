﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Endor.Auth.Models.Entities
{
    public enum AuthenticationType : byte
    {
        Password = 1,
        Google = 2,
        Office365 = 3
    }

    public partial class EnumAuthenticationType
    {
        [Column("ID", TypeName = "tinyint")]
        public AuthenticationType ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserCred> UserCreds { get; set; }
    }
}
