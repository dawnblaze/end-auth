﻿using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Entities
{
    public partial class BusinessCustomUrl
    {
        public short ID { get; set; }
        public short BID { get; set; }
        public string CustomUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDT { get; set; }
        public DateTime? InactiveDT { get; set; }
        public bool HasSsl { get; set; }
        public DateTime? SslexpirationDT { get; set; }
        public string MetaData { get; set; }

        public virtual BusinessData B { get; set; }
    }
}
