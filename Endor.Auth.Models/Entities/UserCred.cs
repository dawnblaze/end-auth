﻿using System;

namespace Endor.Auth.Models.Entities
{
    public partial class UserCred
    {
        public int UserID { get; set; }
        public AuthenticationType AuthenticationType { get; set; }
        public string AuthenticationHash { get; set; }
        public DateTime LastUsedDT { get; set; }

        public virtual UserData User { get; set; }
        public virtual EnumAuthenticationType EnumAuthenticationType { get; set; }
    }
}
