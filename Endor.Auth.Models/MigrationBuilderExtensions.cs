﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Auth.Models
{
    public static class MigrationBuilderExtensions
    {
        /// <summary>
        /// Drops a Default Value Constraint if it exists
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        /// <param name="columnName">Name of the column</param>
        public static void DropDefaultConstraintIfExists(this MigrationBuilder migrationBuilder, string tableName, string columnName)
        {
            migrationBuilder.Sql(
                $@"
                    DECLARE @var1 sysname;
                    SELECT @var1 = [d].[name]
                    FROM[sys].[default_constraints][d]
                    INNER JOIN[sys].[columns][c] ON[d].[parent_column_id] = [c].[column_id] AND[d].[parent_object_id] = [c].[object_id]
                    WHERE([d].[parent_object_id] = OBJECT_ID(N'[{tableName}]') AND[c].[name] = N'{columnName}');
                    IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [{tableName}] DROP CONSTRAINT [' + @var1 + '];');
                ");
        }
    }
}
