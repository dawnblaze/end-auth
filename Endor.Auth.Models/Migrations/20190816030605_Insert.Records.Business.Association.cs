﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Migrations
{
    public partial class InsertRecordsBusinessAssociation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF NOT EXISTS(SELECT * FROM [dbo].[Business.Association] WHERE ID = 0) 
                    INSERT INTO [dbo].[Business.Association] ([ID],[Name]) VALUES(0,'Independent');
                ELSE
                    UPDATE [dbo].[Business.Association] SET Name = 'Independent' WHERE ID = 0;
                IF NOT EXISTS(SELECT * FROM [dbo].[Business.Association] WHERE ID = 1) 
                    INSERT INTO [dbo].[Business.Association] ([ID],[Name]) VALUES(1,'Fastsigns');
                ELSE
                    UPDATE [dbo].[Business.Association] SET Name = 'Fastsigns' WHERE ID = 1;
                IF NOT EXISTS(SELECT * FROM [dbo].[Business.Association] WHERE ID = 2) 
                    INSERT INTO [dbo].[Business.Association] ([ID],[Name]) VALUES(2,'Alliance Franchise Brands');
                ELSE
                    UPDATE [dbo].[Business.Association] SET Name = 'Alliance Franchise Brands' WHERE ID = 2;
                IF NOT EXISTS(SELECT * FROM [dbo].[Business.Association] WHERE ID = 3) 
                    INSERT INTO [dbo].[Business.Association] ([ID],[Name]) VALUES(3,'SignWorld');
                ELSE
                    UPDATE [dbo].[Business.Association] SET Name = 'SignWorld' WHERE ID = 3;
                IF NOT EXISTS(SELECT * FROM [dbo].[Business.Association] WHERE ID = 4) 
                    INSERT INTO [dbo].[Business.Association] ([ID],[Name]) VALUES(4,'Sign*A*Rama');
                ELSE
                    UPDATE [dbo].[Business.Association] SET Name = 'Sign*A*Rama' WHERE ID = 4;
                IF NOT EXISTS(SELECT * FROM [dbo].[Business.Association] WHERE ID = 5) 
                    INSERT INTO [dbo].[Business.Association] ([ID],[Name]) VALUES(5,'Speedpro US');
                ELSE
                    UPDATE [dbo].[Business.Association] SET Name = 'Speedpro US' WHERE ID = 5;                
                IF NOT EXISTS(SELECT * FROM [dbo].[Business.Association] WHERE ID = 6) 
                    INSERT INTO [dbo].[Business.Association] ([ID],[Name]) VALUES(6,'Speedpro CA');
                ELSE
                    UPDATE [dbo].[Business.Association] SET Name = 'Speedpro CA' WHERE ID = 6;                
                IF NOT EXISTS(SELECT * FROM [dbo].[Business.Association] WHERE ID = 11) 
                    INSERT INTO [dbo].[Business.Association] ([ID],[Name]) VALUES(11,'AlphaGraphics');
                ELSE
                    UPDATE [dbo].[Business.Association] SET Name = 'AlphaGraphics' WHERE ID = 11;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
