﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Migrations
{
    public partial class BusinessDataServerData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "AnalyticsServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ApiServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "BackgroundEngineServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "BoardServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "LogServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "MessageServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "PaymentServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ReportServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "SearchServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "StorageServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "WebServerID",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "enum.Server.Type",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Server.Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Server.Data",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(255)", nullable: true),
                    ServerType = table.Column<byte>(type: "tinyint", nullable: false),
                    URL = table.Column<string>(type: "varchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Server.Data", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Server.Data_enum.Server.Type",
                        column: x => x.ServerType,
                        principalTable: "enum.Server.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });
            
            migrationBuilder.CreateIndex(
                name: "IX_Server.Data_ServerType",
                table: "Server.Data",
                column: "ServerType");

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data7",
                table: "Business.Data",
                column: "AnalyticsServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data1",
                table: "Business.Data",
                column: "ApiServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data8",
                table: "Business.Data",
                column: "BackgroundEngineServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data5",
                table: "Business.Data",
                column: "BoardServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data4",
                table: "Business.Data",
                column: "LogServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data2",
                table: "Business.Data",
                column: "MessageServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data9",
                table: "Business.Data",
                column: "PaymentServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data6",
                table: "Business.Data",
                column: "ReportServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data3",
                table: "Business.Data",
                column: "SearchServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data10",
                table: "Business.Data",
                column: "StorageServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data11",
                table: "Business.Data",
                column: "WebServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
INSERT INTO [enum.Server.Type] (ID, Name)
VALUES (0, 'Web')
     , (1, 'API')
     , (2, 'BGEngine')
     , (3, 'DocumentReports')
     , (4, 'AnalyticReports')
     , (5, 'Search')
     , (6, 'Auth')
     , (7, 'Log')
     , (8, 'Board')
     , (21, 'APE')
     , (22, 'ATE')
     , (31, 'Storage')
     , (32, 'Message')
;

INSERT INTO [Server.Data] (ServerType, Name, URL)
    SELECT DISTINCT ST.ID, ST.Name+'1', B.WebServerURL FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 0
    
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.APIServerURL FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 1
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.BackgroundEngineServerUrl FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 2
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.ReportServerURL FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 3
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.AnalyticsServerUrl FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 4
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.SearchServerURL FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 5
​
    --UNION ALL 
    --SELECT DISTINCT ST.ID, ST.Name+'1', B.Auth FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 6
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.LogServerURL FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 7
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.BoardServerURL FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 8
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.PaymentServerUrl FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 21
​
    --UNION ALL 
    --SELECT DISTINCT ST.ID, ST.Name+'1', B.TaxServerUrl FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 22
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.StorageURL FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 31
​
    UNION ALL 
    SELECT DISTINCT ST.ID, ST.Name+'1', B.MessageServerURL FROM [Business.Data] B JOIN [enum.Server.Type] ST ON ST.ID = 32
    ;

UPDATE B SET B.WebServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 0 AND B.WebServerURL = S.URL
UPDATE B SET B.APIServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 1 AND B.APIServerURL = S.URL
UPDATE B SET B.BackgroundEngineServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 2 AND B.BackgroundEngineServerURL = S.URL
UPDATE B SET B.ReportServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 3 AND B.ReportServerURL = S.URL
UPDATE B SET B.AnalyticsServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 4 AND B.AnalyticsServerURL = S.URL
UPDATE B SET B.SearchServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 5 AND B.SearchServerURL = S.URL
--UPDATE B SET B.AuthServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 6 AND B.AuthServerURL = S.URL
UPDATE B SET B.LogServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 7 AND B.LogServerURL = S.URL
UPDATE B SET B.BoardServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 8 AND B.BoardServerURL = S.URL
UPDATE B SET B.PaymentServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 21 AND B.PaymentServerURL = S.URL
--UPDATE B SET B.TaxServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 22 AND B.TaxServerURL = S.URL
UPDATE B SET B.StorageServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 31 AND B.StorageURL = S.URL
UPDATE B SET B.MessageServerID = S.ID FROM [Business.Data] B INNER JOIN [Server.Data] S ON S.ServerType = 32 AND B.MessageServerURL = S.URL
            ");

            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "AnalyticsServerURL");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "BoardServerURL");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "BackgroundEngineServerURL");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "PaymentServerURL");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "ReportServerURL");


            migrationBuilder.DropColumn(
                name: "AnalyticsServerUrl",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "APIServerURL",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "BackgroundEngineServerUrl",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "BoardServerURL",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "LogServerURL",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "MessageServerURL",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "PaymentServerUrl",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ReportServerURL",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "SearchServerURL",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "StorageURL",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "WebServerURL",
                table: "Business.Data");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data7",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data1",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data8",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data5",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data4",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data2",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data9",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data6",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data3",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data10",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data11",
                table: "Business.Data");

            migrationBuilder.DropTable(
                name: "Server.Data");

            migrationBuilder.DropTable(
                name: "enum.Server.Type");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_AnalyticsServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ApiServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_BackgroundEngineServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_BoardServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_LogServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_MessageServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_PaymentServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ReportServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_SearchServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_StorageServerID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_WebServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "AnalyticsServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ApiServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "BackgroundEngineServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "BoardServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "LogServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "MessageServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "PaymentServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ReportServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "SearchServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "StorageServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "WebServerID",
                table: "Business.Data");

            migrationBuilder.AddColumn<string>(
                name: "AnalyticsServerUrl",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "APIServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "BackgroundEngineServerUrl",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "BoardServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "LogServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "MessageServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PaymentServerUrl",
                table: "Business.Data",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReportServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "SearchServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "StorageURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "WebServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                computedColumnSql: "CASE WHEN (CHARINDEX('localhost', WebServerDomain) > 0) THEN CONCAT('https://', WebServerDomain) ELSE CONCAT('https://', BusinessAppName, '.', WebServerDomain) END");
        }
    }
}
