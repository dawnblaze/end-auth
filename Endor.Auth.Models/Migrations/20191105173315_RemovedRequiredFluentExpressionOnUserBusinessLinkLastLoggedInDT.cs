﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Auth.Models.Migrations
{
    public partial class RemovedRequiredFluentExpressionOnUserBusinessLinkLastLoggedInDT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "LastLoggedInDT",
                table: "User.BusinessLink",
                type: "datetime2(2)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "LastLoggedInDT",
                table: "User.BusinessLink",
                type: "datetime2(2)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldNullable: true);
        }
    }
}
