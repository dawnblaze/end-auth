﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Auth.Models.Migrations
{
    public partial class CreateServerLinkTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Business.ServerLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ServerID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business.ServerLink", x => new { x.BID, x.ServerID });
                    table.ForeignKey(
                        name: "FK_Business.ServerLink_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Business.ServerLink_Server.Data",
                        column: x => x.ServerID,
                        principalTable: "Server.Data",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.Sql(@"INSERT INTO [Business.ServerLink]
                SELECT BID, BusinessDatabaseID FROM[Business.Data]
                UNION SELECT BID, AnalyticsServerID FROM[Business.Data]
                UNION SELECT BID, ApiServerID FROM[Business.Data]
                UNION SELECT BID, BackgroundEngineServerID FROM[Business.Data]
                UNION SELECT BID, BoardServerID FROM[Business.Data]
                UNION SELECT BID, LogServerID FROM[Business.Data]
                UNION SELECT BID, MessageServerID FROM[Business.Data]
                UNION SELECT BID, PaymentServerID FROM[Business.Data]
                UNION SELECT BID, ReportServerID FROM[Business.Data]
                UNION SELECT BID, SearchServerID FROM[Business.Data]
                UNION SELECT BID, StorageServerID FROM[Business.Data]
                UNION SELECT BID, WebServerID FROM[Business.Data]
                UNION SELECT BID, ATEServerID FROM[Business.Data]
            ;");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data7",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data1",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data12",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data8",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data5",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data4",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data2",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data9",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data6",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data3",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data10",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data11",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_AteServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "AnalyticsServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ApiServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "AteServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "BackgroundEngineServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "BoardServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "LogServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "MessageServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "PaymentServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ReportServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "SearchServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "StorageServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "WebServerID",
                table: "Business.Data");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID1",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID10",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID11",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID2",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID3",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID4",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID5",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID6",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID7",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID8",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data_ServerDataID9",
                table: "Business.Data");

            migrationBuilder.DropTable(
                name: "Business.ServerLink");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID1",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID10",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID11",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID2",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID3",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID4",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID5",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID6",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID7",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID8",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_ServerDataID9",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID1",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID10",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID11",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID2",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID3",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID4",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID5",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID6",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID7",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID8",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "ServerDataID9",
                table: "Business.Data");

            migrationBuilder.AddColumn<short>(
                name: "AnalyticsServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "ApiServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "AteServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "BackgroundEngineServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "BoardServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "LogServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "MessageServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "PaymentServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "ReportServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "SearchServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "StorageServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "WebServerID",
                table: "Business.Data",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_AnalyticsServerID",
                table: "Business.Data",
                column: "AnalyticsServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_ApiServerID",
                table: "Business.Data",
                column: "ApiServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_AteServerID",
                table: "Business.Data",
                column: "AteServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_BackgroundEngineServerID",
                table: "Business.Data",
                column: "BackgroundEngineServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_BoardServerID",
                table: "Business.Data",
                column: "BoardServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_LogServerID",
                table: "Business.Data",
                column: "LogServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_MessageServerID",
                table: "Business.Data",
                column: "MessageServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_PaymentServerID",
                table: "Business.Data",
                column: "PaymentServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_ReportServerID",
                table: "Business.Data",
                column: "ReportServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_SearchServerID",
                table: "Business.Data",
                column: "SearchServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_StorageServerID",
                table: "Business.Data",
                column: "StorageServerID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_WebServerID",
                table: "Business.Data",
                column: "WebServerID");

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data7",
                table: "Business.Data",
                column: "AnalyticsServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data1",
                table: "Business.Data",
                column: "ApiServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data12",
                table: "Business.Data",
                column: "AteServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data8",
                table: "Business.Data",
                column: "BackgroundEngineServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data5",
                table: "Business.Data",
                column: "BoardServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data4",
                table: "Business.Data",
                column: "LogServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data2",
                table: "Business.Data",
                column: "MessageServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data9",
                table: "Business.Data",
                column: "PaymentServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data6",
                table: "Business.Data",
                column: "ReportServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data3",
                table: "Business.Data",
                column: "SearchServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data10",
                table: "Business.Data",
                column: "StorageServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data11",
                table: "Business.Data",
                column: "WebServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
