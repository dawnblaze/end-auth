﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Migrations
{
    public partial class Initial_Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Commented out to work on pre-existing DB
            //    migrationBuilder.CreateTable(
            //        name: "Business.Association",
            //        columns: table => new
            //        {
            //            ID = table.Column<byte>(type: "tinyint", nullable: false),
            //            Name = table.Column<string>(type: "varchar(255)", nullable: false)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_Business.Association", x => x.ID);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "Client.Device",
            //        columns: table => new
            //        {
            //            ID = table.Column<short>(type: "smallint", nullable: false),
            //            Name = table.Column<string>(type: "varchar(255)", nullable: false),
            //            RegEx = table.Column<string>(type: "varchar(max)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_Client.Device", x => x.ID);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "Database.Data",
            //        columns: table => new
            //        {
            //            ID = table.Column<short>(type: "smallint", nullable: false),
            //            BackupPath = table.Column<string>(type: "varchar(max)", nullable: true),
            //            ConnectionString = table.Column<string>(type: "varchar(max)", nullable: true),
            //            DatabaseName = table.Column<string>(type: "varchar(255)", nullable: true),
            //            IsCustomerDatabase = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "~(([IsSystemDatabase]|[IsReportDatabase])|[IsLoggingDatabase])"),
            //            IsLoggingDatabase = table.Column<bool>(type: "bit", nullable: false),
            //            IsReportDatabase = table.Column<bool>(type: "bit", nullable: false),
            //            IsSystemDatabase = table.Column<bool>(type: "bit", nullable: false),
            //            LastBackupDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
            //            Server = table.Column<string>(type: "varchar(1024)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_Database.Data", x => x.ID);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "enum.Authentication.Type",
            //        columns: table => new
            //        {
            //            ID = table.Column<byte>(type: "tinyint", nullable: false),
            //            Name = table.Column<string>(type: "varchar(255)", nullable: false)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_enum.Authentication.Type", x => x.ID);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "User.Data",
            //        columns: table => new
            //        {
            //            ID = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //            DisplayName = table.Column<string>(type: "varchar(255)", nullable: true),
            //            EmailAddress = table.Column<string>(type: "varchar(255)", nullable: true),
            //            LastPWChangeDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
            //            MustResetPW = table.Column<bool>(type: "bit", nullable: false, defaultValueSql: "1"),
            //            PWResetToken = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
            //            PWResetTokenExpirationDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
            //            PhoneNumber = table.Column<string>(type: "varchar(255)", nullable: true),
            //            UserName = table.Column<string>(type: "varchar(255)", nullable: false)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_User.Data", x => x.ID);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "Business.Data",
            //        columns: table => new
            //        {
            //            BID = table.Column<short>(type: "smallint", nullable: false),
            //            APIServerURL = table.Column<string>(type: "varchar(255)", nullable: false),
            //            AssociationID = table.Column<byte>(type: "tinyint", nullable: false, defaultValueSql: "0"),
            //            BoardServerURL = table.Column<string>(type: "varchar(255)", nullable: false),
            //            BusinessAppName = table.Column<string>(type: "varchar(32)", nullable: false),
            //            BusinessDatabaseID = table.Column<short>(type: "smallint", nullable: false),
            //            CompanyName = table.Column<string>(type: "varchar(255)", nullable: false),
            //            CreatedDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false, defaultValueSql: "getutcdate()"),
            //            InactiveDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
            //            IndexStorageConnectionString = table.Column<string>(type: "varchar(512)", nullable: false),
            //            IsActive = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "isnull(case when [InactiveDT] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))"),
            //            LogServerURL = table.Column<string>(type: "varchar(255)", nullable: false),
            //            LoggingDatabaseID = table.Column<short>(type: "smallint", nullable: false),
            //            MessageServerURL = table.Column<string>(type: "varchar(255)", nullable: false),
            //            NodeID = table.Column<byte>(type: "tinyint", nullable: false),
            //            ReportDatabaseID = table.Column<short>(type: "smallint", nullable: false),
            //            ReportServerURL = table.Column<string>(type: "varchar(255)", nullable: false),
            //            SearchServerURL = table.Column<string>(type: "varchar(255)", nullable: false),
            //            StorageConnectionString = table.Column<string>(type: "varchar(512)", nullable: false),
            //            StorageURL = table.Column<string>(type: "varchar(255)", nullable: false),
            //            SystemDatabaseID = table.Column<short>(type: "smallint", nullable: false),
            //            WebServerURL = table.Column<string>(type: "varchar(255)", nullable: false)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_Business", x => x.BID);
            //            table.ForeignKey(
            //                name: "FK_Business.Data_Business.Association",
            //                column: x => x.AssociationID,
            //                principalTable: "Business.Association",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_Business.Data_Database.Data1",
            //                column: x => x.BusinessDatabaseID,
            //                principalTable: "Database.Data",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_Business.Data_Database.Data4",
            //                column: x => x.LoggingDatabaseID,
            //                principalTable: "Database.Data",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_Business.Data_Database.Data3",
            //                column: x => x.ReportDatabaseID,
            //                principalTable: "Database.Data",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_Business.Data_Database.Data2",
            //                column: x => x.SystemDatabaseID,
            //                principalTable: "Database.Data",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "User.Cred",
            //        columns: table => new
            //        {
            //            UserID = table.Column<int>(type: "int", nullable: false),
            //            AuthenticationType = table.Column<byte>(type: "tinyint", nullable: false),
            //            AuthenticationHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            LastUsedDT = table.Column<DateTime>(type: "datetime2", nullable: false)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_User.Cred", x => new { x.UserID, x.AuthenticationType });
            //            table.ForeignKey(
            //                name: "FK_User.Cred_enum.Authentication.Type",
            //                column: x => x.AuthenticationType,
            //                principalTable: "enum.Authentication.Type",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_User.Cred_User.Data",
            //                column: x => x.UserID,
            //                principalTable: "User.Data",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "Business.CustomURL",
            //        columns: table => new
            //        {
            //            ID = table.Column<short>(type: "smallint", nullable: false)
            //                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //            BID = table.Column<short>(type: "smallint", nullable: false),
            //            CreatedDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false, defaultValueSql: "getutcdate()"),
            //            CustomURL = table.Column<string>(type: "varchar(255)", nullable: false),
            //            HasSSL = table.Column<bool>(type: "bit", nullable: false, defaultValueSql: "0"),
            //            InactiveDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
            //            IsActive = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "isnull(case when [InactiveDT] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))"),
            //            MetaData = table.Column<string>(type: "xml", nullable: true),
            //            SSLExpirationDT = table.Column<DateTime>(type: "datetime2", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_Business.CustomURL", x => x.ID);
            //            table.ForeignKey(
            //                name: "FK_Business.CustomURL_Business.Data",
            //                column: x => x.BID,
            //                principalTable: "Business.Data",
            //                principalColumn: "BID",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "Client.Data",
            //        columns: table => new
            //        {
            //            ID = table.Column<long>(type: "bigint", nullable: false)
            //                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //            BID = table.Column<short>(type: "smallint", nullable: true),
            //            ClientID = table.Column<string>(type: "varchar(255)", nullable: false),
            //            ConnectDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "getutcdate()"),
            //            DeviceID = table.Column<short>(type: "smallint", nullable: true),
            //            InactiveMinutes = table.Column<int>(type: "int", nullable: true, computedColumnSql: "datediff(minute,[LastActivityDT],getutcdate())"),
            //            LastActivityDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
            //            UserID = table.Column<int>(type: "int", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_Client.Data", x => x.ID);
            //            table.ForeignKey(
            //                name: "FK_Client.Connection_Business.Data",
            //                column: x => x.BID,
            //                principalTable: "Business.Data",
            //                principalColumn: "BID",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_Client.Connection_Client.Device",
            //                column: x => x.DeviceID,
            //                principalTable: "Client.Device",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_Client.Connection_User.Data",
            //                column: x => x.UserID,
            //                principalTable: "User.Data",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "User.BusinessLink",
            //        columns: table => new
            //        {
            //            BID = table.Column<short>(type: "smallint", nullable: false),
            //            UserID = table.Column<int>(type: "int", nullable: false),
            //            LastLoggedInDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_User.BusinessLink", x => new { x.BID, x.UserID });
            //            table.ForeignKey(
            //                name: "FK_User.BusinessLink_Business.Data",
            //                column: x => x.BID,
            //                principalTable: "Business.Data",
            //                principalColumn: "BID",
            //                onDelete: ReferentialAction.Cascade);
            //            table.ForeignKey(
            //                name: "PK_User.BusinessLink",
            //                column: x => x.UserID,
            //                principalTable: "User.Data",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "User.Invite",
            //        columns: table => new
            //        {
            //            ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
            //            BID = table.Column<short>(type: "smallint", nullable: false),
            //            DisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            EmailAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            ExpirationDT = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            IsEmployee = table.Column<bool>(type: "bit", nullable: false),
            //            PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_User.Invite", x => x.ID);
            //            table.ForeignKey(
            //                name: "FK_User.Invite_Business.Data",
            //                column: x => x.BID,
            //                principalTable: "Business.Data",
            //                principalColumn: "BID",
            //                onDelete: ReferentialAction.Cascade);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "User.RefreshToken",
            //        columns: table => new
            //        {
            //            ID = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //            BID = table.Column<short>(type: "smallint", nullable: false),
            //            ClientID = table.Column<string>(type: "varchar(1024)", nullable: true),
            //            ExpiresDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GETUTCDATE()"),
            //            IssuedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GETUTCDATE() + (32)"),
            //            LastUsedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
            //            Nonce = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "NEWID()"),
            //            RevokedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
            //            Token = table.Column<string>(type: "varchar(255)", nullable: true),
            //            UserID = table.Column<int>(type: "int", nullable: false)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_User.RefreshToken", x => x.ID);
            //            table.ForeignKey(
            //                name: "FK_User.RefreshToken_Business.Data",
            //                column: x => x.BID,
            //                principalTable: "Business.Data",
            //                principalColumn: "BID",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_User.RefreshToken_User.Data",
            //                column: x => x.UserID,
            //                principalTable: "User.Data",
            //                principalColumn: "ID",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.CustomURL_BID",
            //        table: "Business.CustomURL",
            //        columns: new[] { "BID", "CustomURL", "IsActive" });

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.CustomURL_URL",
            //        table: "Business.CustomURL",
            //        columns: new[] { "CustomURL", "BID", "IsActive" });

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.Data_AssociationID",
            //        table: "Business.Data",
            //        column: "AssociationID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.Data_LoggingDatabaseID",
            //        table: "Business.Data",
            //        column: "LoggingDatabaseID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.Data_ReportDatabaseID",
            //        table: "Business.Data",
            //        column: "ReportDatabaseID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.Data_SystemDatabaseID",
            //        table: "Business.Data",
            //        column: "SystemDatabaseID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.Data_AppName",
            //        table: "Business.Data",
            //        columns: new[] { "BusinessAppName", "IsActive" });

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.Data_DatabaseID",
            //        table: "Business.Data",
            //        columns: new[] { "BusinessDatabaseID", "IsActive" });

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Business.Data_CompanyName",
            //        table: "Business.Data",
            //        columns: new[] { "CompanyName", "IsActive" });

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Client.Connection_BID",
            //        table: "Client.Data",
            //        column: "BID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Client.Connection_Client",
            //        table: "Client.Data",
            //        column: "ClientID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Client.Data_DeviceID",
            //        table: "Client.Data",
            //        column: "DeviceID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Client.Data_UserID",
            //        table: "Client.Data",
            //        column: "UserID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.BusinessLink_UserID",
            //        table: "User.BusinessLink",
            //        column: "UserID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.Cred_AuthenticationType",
            //        table: "User.Cred",
            //        column: "AuthenticationType");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.Data_EmailAddess",
            //        table: "User.Data",
            //        column: "EmailAddress");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.Data_UserName",
            //        table: "User.Data",
            //        column: "UserName");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.Invite_BID",
            //        table: "User.Invite",
            //        column: "BID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.RefreshToken_Nonce",
            //        table: "User.RefreshToken",
            //        column: "Nonce");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.RefreshToken_Token",
            //        table: "User.RefreshToken",
            //        column: "Token");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.RefreshToken_UserID",
            //        table: "User.RefreshToken",
            //        column: "UserID");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_User.RefreshToken_BID_UserID",
            //        table: "User.RefreshToken",
            //        columns: new[] { "BID", "UserID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Business.CustomURL");

            migrationBuilder.DropTable(
                name: "Client.Data");

            migrationBuilder.DropTable(
                name: "User.BusinessLink");

            migrationBuilder.DropTable(
                name: "User.Cred");

            migrationBuilder.DropTable(
                name: "User.Invite");

            migrationBuilder.DropTable(
                name: "User.RefreshToken");

            migrationBuilder.DropTable(
                name: "Client.Device");

            migrationBuilder.DropTable(
                name: "enum.Authentication.Type");

            migrationBuilder.DropTable(
                name: "Business.Data");

            migrationBuilder.DropTable(
                name: "User.Data");

            migrationBuilder.DropTable(
                name: "Business.Association");

            migrationBuilder.DropTable(
                name: "Database.Data");
        }
    }
}
