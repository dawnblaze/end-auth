﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Migrations
{
    public partial class AddBackgroundEngineAndAnalyticsUrls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AnalyticsServerUrl",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "BackgroundEngineServerUrl",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnalyticsServerUrl",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "BackgroundEngineServerUrl",
                table: "Business.Data");
        }
    }
}
