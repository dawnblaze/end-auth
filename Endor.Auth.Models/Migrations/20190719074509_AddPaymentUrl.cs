﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Migrations
{
    public partial class AddPaymentUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PaymentServerUrl",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.Sql(@"
                UPDATE [Business.Data] SET PaymentServerUrl = 'http://endorpayment.localcyriousdevelopment.com:5011/'
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentServerUrl",
                table: "Business.Data");
        }
    }
}
