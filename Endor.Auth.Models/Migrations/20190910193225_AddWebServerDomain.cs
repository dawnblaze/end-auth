﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.Auth.Models.Migrations
{
    public partial class AddWebServerDomain : Migration
    {
        private const string protocolPrefix = "https://";

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "WebServerDomain",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.Sql($@"
    --copy existing WebServerURL into WebServerDomain, but strip https:// off of it

    UPDATE [Business.Data] SET WebServerDomain = CASE WHEN CHARINDEX('{protocolPrefix}',WebServerURL) = 1 THEN RIGHT(WebServerURL, LEN(WebServerURL) - LEN('{protocolPrefix}')) ELSE WebServerURL END; 
");

            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "WebServerURL");

            migrationBuilder.AlterColumn<string>(
                name: "WebServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                computedColumnSql: "CASE WHEN (CHARINDEX('localhost', WebServerDomain) > 0) THEN CONCAT('https://', WebServerDomain) ELSE CONCAT('https://', BusinessAppName, '.', WebServerDomain) END",
                oldNullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // this won't work because EF has a bug, it can't alter computed columns
            // https://github.com/aspnet/EntityFrameworkCore/issues/14075
            //migrationBuilder.AlterColumn<string>(
            //    name: "WebServerURL",
            //    table: "Business.Data",
            //    type: "varchar(255)",
            //    nullable: false,
            //    oldClrType: typeof(string),
            //    oldType: "varchar(255)",
            //    oldComputedColumnSql: "CASE WHEN (CHARINDEX('localhost', WebServerDomain) > 0) THEN WebServerDomain ELSE CONCAT('https://', BusinessAppName, '.', WebServerDomain) END");

            migrationBuilder.DropColumn(
                name: "WebServerURL",
                table: "Business.Data");

            migrationBuilder.AddColumn<string>(
                name: "WebServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.Sql($@"
    --copy WebServerDomain into WebServerURL and add https:// back to it

    UPDATE [Business.Data] SET WebServerURL = CONCAT('{protocolPrefix}', WebServerDomain);
");

            migrationBuilder.AlterColumn<string>(
                name: "WebServerURL",
                table: "Business.Data",
                type: "varchar(255)",
                nullable: false,
                oldDefaultValue: "");

            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "WebServerDomain");

            migrationBuilder.DropColumn(
                name: "WebServerDomain",
                table: "Business.Data");

        }
    }
}
