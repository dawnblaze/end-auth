﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Auth.Models.Migrations
{
    public partial class AddAteServerData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "AteServerID",
                table: "Business.Data",
                nullable: false,
                defaultValue: (short)1);

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_AteServerID",
                table: "Business.Data",
                column: "AteServerID");

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Data_Server.Data12",
                table: "Business.Data",
                column: "AteServerID",
                principalTable: "Server.Data",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Server.Data12",
                table: "Business.Data");

            migrationBuilder.DropIndex(
                name: "IX_Business.Data_AteServerID",
                table: "Business.Data");

            migrationBuilder.DropColumn(
                name: "AteServerID",
                table: "Business.Data");
        }
    }
}
