﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Auth.Models.Migrations
{
    public partial class UpdateServerTypeEnums : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [enum.Server.Type] 
                SET Name = 'RTC'
                WHERE ID = 32
                ;

                INSERT INTO [enum.Server.Type] ([ID], [Name])
                VALUES (33, N'Message (Email)')
                     , (23, N'APSA')
                     , (34, N'Merge Fields')
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [enum.Server.Type] 
                SET Name = 'Messaging'
                WHERE ID = 32
                ;

                DELETE FROM [enum.Server.Type]
                WHERE ID=33 OR ID=23 OR ID=34
            ");

        }
    }
}
