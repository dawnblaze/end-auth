﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
using System.Security.Principal;
using System.IO;
using Endor.Auth.Web.Models;
using System.Text;
using Endor.Auth.Web.Classes.Encoding;
using Microsoft.AspNetCore.Authentication;
using Endor.Auth.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace Endor.Auth.Web.Middleware
{
    public class TokenProviderMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly TokenProviderOptions _options;

        public TokenProviderMiddleware(RequestDelegate next, IOptions<TokenProviderOptions> options)
        {
            _next = next;
            _options = options.Value;
        }

        public Task Invoke(HttpContext context)
        {
            // If the request path doesn't match, skip
            if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
            {
                return _next(context);
            }

            // Request must be POST 
            if (!context.Request.Method.Equals("POST"))
            {
                context.Response.StatusCode = 405;
                return context.Response.WriteAsync("Method Not Allowed");
            }
            // Request can be with Content-Type: application/x-www-form-urlencoded
            if (context.Request.HasFormContentType)
            {
                return GenerateToken(context);
            }
            // Or with Content-Type: application/json
            if (context.Request.ContentType == "application/json")
            {
                using (StreamReader sr = new StreamReader(context.Request.Body))
                {
                    LoginInput input = JsonConvert.DeserializeObject<LoginInput>(sr.ReadToEnd());

                    return GenerateToken(input, context);
                }
            }
            else
            {
                context.Response.StatusCode = 400;
                return context.Response.WriteAsync("Bad Request");
            }
        }
    }
}
