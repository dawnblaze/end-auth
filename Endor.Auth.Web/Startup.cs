﻿using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Auth.Web.Services;
using Endor.Configuration;
using Endor.Logging.Client;
using Endor.Tenant;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Endor.Auth.Web
{
    public partial class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<AuthContext>(options => options.UseSqlServer(Configuration.GetConnectionString("AuthContext")));
            services.Configure<IISOptions>(options => 
            {
                options.AutomaticAuthentication = true;
            });
            services.Configure<MvcOptions>(options =>
            {
                //options.Filters.Add(new RequireHttpsAttribute());
            });
            services.AddOptions();

            SMTPOptions smtpOptions = Configuration.GetSection("SMTP").Get<SMTPOptions>();
            services.Configure<SMTPOptions>(Configuration.GetSection("SMTP"));
            services.AddSingleton<ISMTPOptions>(smtpOptions);

            EndorAuthOptions appOptions = Configuration.GetSection("Endor").Get<EndorAuthOptions>();
            services.Configure<EndorAuthOptions>(Configuration.GetSection("Endor"));
            services.AddSingleton<Endor.Tenant.ITenantSecretOptions>(appOptions);
            services.AddSingleton<ITenantDataCache, EFTenantDataCache>();

            AuthOptions authOptions = Configuration.GetSection("Auth").Get<AuthOptions>();
            services.Configure<AuthOptions>(Configuration.GetSection("Auth"));
            services.AddSingleton<AuthOptions>(authOptions);
            services.AddSingleton<RemoteLogger>(x => {
                ITenantDataCache cache = x.GetService<ITenantDataCache>();
                RemoteLogger logger = new RemoteLogger(cache);
                logger.TenantSecret = Configuration["Endor:TenantSecret"];
                return logger;
            });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
            {
                option.Audience = Configuration["Auth:ValidAudience"];
                option.TokenValidationParameters = new TokenValidationParameters()
                {
                    NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Auth:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(WebEncoders.Base64UrlDecode(Configuration["Auth:SymmetricKey"])),
                };
            });

            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConfiguration(Configuration.GetSection("Logging"));
                loggingBuilder.AddConsole();
                //loggingBuilder.AddSerilog();
                loggingBuilder.AddDebug();

            });
            services.AddAuthorization();
            services.AddCors();
            services.AddMvc(MvcOptions =>
            {
                MvcOptions.EnableEndpointRouting = false;
            })
            .AddNewtonsoftJson(options =>
            {
                // can be removed. Added for fail-safe
                options.SerializerSettings.ReferenceLoopHandling =
                                            Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                // TODO: SET ACTUAL VALUE
                options.SerializerSettings.MaxDepth = 9999;
            })
            .AddApplicationPart(typeof(Endor.Logging.Client.LogLevelController).GetTypeInfo().Assembly)
            .AddControllersAsServices();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime, IOptions<AuthOptions> authOptions)
        {
            _authOptions = authOptions.Value;
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();
            //loggerFactory.AddConsole();
            RemoteLoggingExtensions.ConfigureSystemRemoteLogging(Configuration, loggerFactory, appLifetime);

            app.UseAuthentication();
            app.UseCors(t => t.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseMvc();
            //var options = new RewriteOptions().AddRedirectToHttps();

            TokenService.FillAudiences(app.ApplicationServices.GetService<AuthContext>());

            ConfigureAuth();
        }        
    }
}
