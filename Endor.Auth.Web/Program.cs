﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Threading;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Net;

namespace Endor.Auth.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Endor.Auth";
            IConfigurationBuilder hostingBuilder = new ConfigurationBuilder()
                .AddEnvironmentVariables();

            var host = new WebHostBuilder();
            var environment = host.GetSetting("environment");
            hostingBuilder.SetBasePath(Directory.GetCurrentDirectory());
            hostingBuilder.AddJsonFile("appsettings.json");
            hostingBuilder.AddJsonFile($"appsettings.{environment}.json", optional: true);

            if (environment == "Development")
            {
                hostingBuilder.AddUserSecrets<Startup>();
            }

            var envConfiguration = hostingBuilder.Build();

            host.UseKestrel(options =>
                {
                    if (environment == "Development")
                    {
                        AddHttpsWithCert(options, envConfiguration);
                    }
                })
                .UseIISIntegration()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                //.UseApplicationInsights()
                .Build()
                .Run();
        }

        private static void AddHttpsWithCert(KestrelServerOptions options, IConfigurationRoot configuration)
        {
            Uri hostUri = new Uri(configuration["ASPNETCORE_URLS"]);
            if (Environment.OSVersion.ToString().Contains("Unix"))
            {
                string wildCardCertPath = "/usr/local/share/ca-certificates/wildcard.localcyriousdevelopment.com.v3.GOES_IN_PERSONAL.pfx";
                string wildCardCertPassword = configuration["wildCardCertPassword"];
                options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(wildCardCertPath, wildCardCertPassword)));
            }
            else
            {
                X509Store store = new X509Store("MY", StoreLocation.LocalMachine);

                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                string thumbprint = configuration["Endor:certThumbprint"];
                X509Certificate2Collection certs = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, true);
                if (certs != null && certs.Count > 0)
                {
                    X509Certificate2 cert = certs[0];

                    options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(cert))); //options.UseHttps(cert);
                }
            }
        }
    }
}
