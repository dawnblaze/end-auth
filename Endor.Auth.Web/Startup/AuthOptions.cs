﻿namespace Endor.Auth.Web
{
    public class AuthOptions
    {
        public string SymmetricKey { get; set; }
        public string ValidIssuer { get; set; }
        public double ExpirationMinutes { get; set; }
    }
}