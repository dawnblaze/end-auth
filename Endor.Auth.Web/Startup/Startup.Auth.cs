﻿using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Middleware;
using Endor.Auth.Web.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Endor.Auth.Web
{
    public partial class Startup
    {
        private AuthOptions _authOptions;
        public void ConfigureAuth()
        {
            string symmetricKeyAsBase64 = _authOptions.SymmetricKey;

            byte[] keyByteArray = Base64UrlTextEncoder.Decode(symmetricKeyAsBase64);

            SymmetricSecurityKey signingKey = new SymmetricSecurityKey(keyByteArray);

            TokenProviderOptions tokenProviderOptions = new TokenProviderOptions
            {
                Issuer = GetIssuer(),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
                Expiration = TimeSpan.FromMinutes(_authOptions.ExpirationMinutes <= 0 ? 263520 : _authOptions.ExpirationMinutes),
            };

            TokenService.Options = tokenProviderOptions;
        }

        private string GetIssuer()
        {
            return _authOptions.ValidIssuer;
        }
    }
}
