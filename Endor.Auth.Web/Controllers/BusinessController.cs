﻿using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Auth.Web.Models;
using Endor.Auth.Web.Services;
using Endor.Configuration;
using Endor.Logging.Client;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Controllers
{
    [Route("api/business")]
    public class BusinessController: Controller
    {
        /// <summary>
        /// DB authorization context
        /// </summary>
        private readonly AuthContext _authContext;
        /// <summary>
        /// Authorization options
        /// </summary>
        private readonly EndorAuthOptions _options;
        /// <summary>
        /// Remote logger
        /// </summary>
        private readonly RemoteLogger _logger;
        /// <summary>
        /// Tenant's Secret Options
        /// </summary>
        private Endor.Tenant.ITenantSecretOptions _secretOptions;
        /// <summary>
        /// SMTP Options for sending emails
        /// </summary>
        private readonly IOptions<SMTPOptions> _smtpOptions;

        /// <summary>
        /// Auth API's Account controller
        /// </summary>
        /// <param name="authContext">DB authorization context</param>
        /// <param name="options">Authorization options</param>
        /// <param name="logger">Remote logger</param>
        public BusinessController(AuthContext authContext, IOptions<EndorAuthOptions> options, RemoteLogger logger, ITenantSecretOptions secretOptions, IOptions<SMTPOptions> smtpOptions)
        {
            _authContext = authContext;
            _options = options.Value;
            _logger = logger;
            _secretOptions = secretOptions;
            _smtpOptions = smtpOptions;
        }

        /// <summary>
        /// Creates a new Business
        /// </summary>
        /// <param name="TemplateBID">Template BID</param>
        /// <param name="FirstName">The first name of the owner</param>
        /// <param name="LastName">The last name of the owner</param>
        /// <param name="EmailAddress">The email address of the owner.  The new login link will be sent to this email address</param>
        /// <param name="LocationName">The name of the first location for the Business</param>
        /// <param name="NewBusinessID">The BID to use</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateBusiness([FromQuery] short TemplateBID, [FromQuery] string FirstName, [FromQuery] string LastName, [FromQuery] string EmailAddress, [FromQuery] string LocationName, [FromQuery] short? NewBusinessID)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            try
            {
                BusinessService service = new BusinessService(_authContext, _options, _logger, _smtpOptions);
                CreateBusinessResponse resp = await service.CreateAsync(TemplateBID, FirstName, LastName, EmailAddress, LocationName, NewBusinessID, HttpContext.User.Identity);

                if (resp.Success)
                    return Ok(resp);

                return BadRequest(resp);

            }
            catch (Exception err)
            {
                return StatusCode(500, err);
            }

        }

        /// <summary>
        /// Clones an existing Business to a new Business
        /// </summary>
        /// <param name="ID">ID of the Business to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        public async Task<IActionResult> CloneAsync(short ID)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            try
            {
                BusinessService service = new BusinessService(_authContext, _options, _logger, _smtpOptions);
                GenericResponse resp = await service.CloneAsync(ID);

                if (resp.Success)
                    return Ok(resp);

                return BadRequest(resp);

            }
            catch (Exception err)
            {
                return StatusCode(500, err);
            }
        }

        /// <summary>
        /// Deletes a Business by ID with an optional TestOnly query param.
        /// </summary>
        /// <param name="ID">ID of the Business to Delete</param>
        /// <param name="TestOnly">When false, it rollback delete otherwise it's deleted.</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        public async Task<IActionResult> Delete(short ID, [FromQuery] bool? TestOnly = false)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            try
            {
                BusinessService service = new BusinessService(_authContext, _options, _logger, _smtpOptions);
                GenericResponse resp = await service.Delete(ID, TestOnly);

                if (resp.Success)
                    return Ok(resp);

                return BadRequest(resp);

            }
            catch (Exception err)
            {
                return StatusCode(500, err);
            }
        }

        /// <summary>
        /// Reloads the list of businesses
        /// </summary>
        /// <returns></returns>
        [HttpPost("reload")]
        public async Task<IActionResult> ReloadBusinesses()
        {
            TokenService service = new TokenService(_authContext, _options, _logger);
            await service.ReloadAudiences();
            return Ok();
        }

        /// <summary>
        /// Reloads the list of businesses
        /// </summary>
        /// <returns></returns>
        [HttpPost("{ID}/reload")]
        public async Task<IActionResult> ReloadBusinesses(short ID)
        {
            TokenService service = new TokenService(_authContext, _options, _logger);
            await service.ReloadAudiences(ID);
            return Ok();
        }

        /// <summary>
        /// Sets a Business to to active
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        public async Task<IActionResult> SetActive(short ID)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            BusinessService service = new BusinessService(_authContext, _options, _logger, _smtpOptions);
            var resp = await service.SetActive(ID, true);

            if (resp.Success)
                return Ok(resp);

            return BadRequest(resp);
        }

        /// <summary>
        /// Sets a Business to to Inactive
        /// </summary>
        /// <param name="ID">Business ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        public async Task<IActionResult> SetInactive(short ID)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            BusinessService service = new BusinessService(_authContext, _options, _logger, _smtpOptions);
            var resp = await service.SetActive(ID, false);

            if (resp.Success)
                return Ok(resp);

            return BadRequest(resp);
        }

        #region Private Helpers

        private bool SecretMatches(string auth)
        {
            if (auth == null || !auth.StartsWith("Bearer "))
            {
                return false;
            }
            else
            {
                string value = auth.Substring("Bearer ".Length);
                try
                {
                    return Encoding.UTF8.GetString(Convert.FromBase64String(value)) == _secretOptions.TenantSecret;
                }
                catch
                {
                    return false;
                }
            }
        }

        #endregion
    }
}
