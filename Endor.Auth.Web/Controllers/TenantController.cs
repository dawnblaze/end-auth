using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Controllers
{
    [Produces("application/json")]
    [Route("tenant")]
    public class TenantController : Controller
    {
        private readonly AuthContext _authContext;
        private Endor.Tenant.ITenantSecretOptions _secretOptions;

        public TenantController(AuthContext authContext, Endor.Tenant.ITenantSecretOptions secretOptions)
        {
            _authContext = authContext;
            _secretOptions = secretOptions;
        }

        [HttpGet("{bid}")]
        public async Task<IActionResult> Get(short bid)
        {
            if (SecretMatches(HttpContext.Request.Headers["Authorization"]))
            {
                TenantData data = await EFTenantDataCache.GetTenantDataFromDatabase(this._authContext, bid);

                return new OkObjectResult(data);
            }
            else
                return new NotFoundResult();
        }

        private bool SecretMatches(string auth)
        {
            if (auth == null || !auth.StartsWith("Bearer "))
            {
                return false;
            }
            else
            {
                string value = auth.Substring("Bearer ".Length);
                return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(value)) == _secretOptions.TenantSecret;
            }
        }
    }
}