﻿using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Auth.Web.Models;
using Endor.Auth.Web.Services;
using Endor.Configuration;
using Endor.Logging.Client;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Controllers
{
    [Route("[controller]")]
    public class UserController : Controller
    {
        /// <summary>
        /// DB authorization context
        /// </summary>
        private readonly AuthContext _authContext;
        /// <summary>
        /// Authorization options
        /// </summary>
        private readonly EndorAuthOptions _options;
        /// <summary>
        /// Remote logger
        /// </summary>
        private readonly RemoteLogger _logger;
        /// <summary>
        /// Tenant's Secret Options
        /// </summary>
        private Endor.Tenant.ITenantSecretOptions _secretOptions;
        /// <summary>
        /// SMTP Options for sending emails
        /// </summary>
        private readonly IOptions<SMTPOptions> _smtpOptions;

        /// <summary>
        /// Auth API's Account controller
        /// </summary>
        /// <param name="authContext">DB authorization context</param>
        /// <param name="options">Authorization options</param>
        /// <param name="logger">Remote logger</param>
        /// <param name="tenantDataCache">Tenant's data cache</param>
        public UserController(AuthContext authContext, IOptions<EndorAuthOptions> options, RemoteLogger logger, Endor.Tenant.ITenantSecretOptions secretOptions, IOptions<SMTPOptions> smtpOptions)
        {
            _authContext = authContext;
            _options = options.Value;
            _logger = logger;
            _secretOptions = secretOptions;
            _smtpOptions = smtpOptions;
        }

        /// <summary>
        /// API end point for generating invite
        /// </summary>
        /// <param name="inviteRequest">A generate invite request containing a BID and suggested email</param>
        /// <returns>OkResult if successful, BadRequestResult if failure or error</returns>
        [HttpPost("generateinvite")]
        public async Task<IActionResult> GenerateInvite([FromBody]GenerateInviteRequest inviteRequest)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            try
            {
                UserService service = new UserService(_authContext, _options, _logger, _smtpOptions);
                (GenerateInviteRequestResponse, string) resp = await service.InviteUser(inviteRequest);

                if (resp.Item1 != null)
                    return Ok(resp.Item1);

                if (resp.Item2.Contains("already exists", StringComparison.CurrentCultureIgnoreCase))
                    return StatusCode((int)HttpStatusCode.Conflict, resp.Item2);

                return BadRequest(resp.Item2);

            }
            catch (Exception err)
            {
                await _logger.Error(inviteRequest?.BID ?? -1, "Failed to generate invite", err);
                return StatusCode(500, err);
            }
        }

        /// <summary>
        /// API end point for generating invite
        /// </summary>
        /// <param name="inviteRequest">A generate invite request containing a BID and suggested email</param>
        /// <returns>OkResult if successful, BadRequestResult if failure or error</returns>
        [HttpPost("generateinviteforexistinguser")]
        public async Task<IActionResult> GenerateInviteForExistingUser([FromBody]GenerateInviteRequest inviteRequest)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            try
            {
                UserService service = new UserService(_authContext, _options, _logger, _smtpOptions);
                (GenerateInviteRequestResponse, string) resp = await service.InviteUser(inviteRequest, false);

                if (resp.Item1 != null)
                    return Ok(resp.Item1);

                if (resp.Item2.Contains("already exists", StringComparison.CurrentCultureIgnoreCase))
                    return StatusCode((int)HttpStatusCode.Conflict, resp.Item2);

                return BadRequest(resp.Item2);

            }
            catch (Exception err)
            {
                await _logger.Error(inviteRequest?.BID ?? -1, "Failed to generate invite", err);
                return StatusCode(500, err);
            }
        }

        /// <summary>
        ///  Removes the matching invite
        /// </summary>
        /// <param name="inviteRequest">A revoke invite request containing the GUID of the invite</param>
        /// <returns>OkResult if successful, BadRequestResult if failure or error</returns>
        [HttpPost("revokeinvite")]
        public async Task<IActionResult> RevokeInvite([FromBody]RevokeInviteRequest revokeInviteRequest)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            try
            {
                UserService service = new UserService(_authContext, _options, _logger, _smtpOptions);
                (bool, string) resp = await service.RevokeUserInvite(revokeInviteRequest);

                if (resp.Item1)
                    return Ok(resp.Item1);

                return BadRequest(resp.Item2);

            }
            catch (Exception err)
            {
                await _logger.Error(-1, "Failed to revoke invite", err);
                return StatusCode(500, err);
            }
        }

        /// <summary>
        ///  Removes the business link for a user
        /// </summary>
        /// <param name="revokeAccessRequest">A revoke user business access request containing the BID and UserId</param>
        /// <returns>OkResult if successful, BadRequestResult if failure or error</returns>
        [HttpPost("revokeaccess")]
        public async Task<IActionResult> RevokeAccess([FromBody]UserRevokeAccessRequest revokeAccessRequest)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            try
            {
                UserService service = new UserService(_authContext, _options, _logger, _smtpOptions);
                (bool, string) resp = await service.RevokeUserBusinessAccess(revokeAccessRequest);

                if (resp.Item1)
                    return Ok(resp.Item1);

                return BadRequest(resp.Item2);

            }
            catch (Exception err)
            {
                await _logger.Error(revokeAccessRequest?.BID ?? -1, "Failed to revoke access", err);
                return StatusCode(500, err);
            }
        }

        /// <summary>
        ///  Makes a temporary User into a permanent user
        /// </summary>
        /// <param name="GUID">Tenant Secret Key</param>
        /// <param name="userId">User Id</param>
        /// <returns>OkResult if successful, BadRequestResult if failure or error</returns>
        [HttpPost("{GUID}/{userid}")]
        public async Task<IActionResult> ConvertUserToPermanent(Guid GUID, int userId)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            try
            {
                UserService service = new UserService(_authContext, _options, _logger, _smtpOptions);
                (bool, string) resp = await service.ConvertUserToPermanentUser(GUID,userId);

                if (resp.Item1)
                    return Ok(resp.Item1);

                return BadRequest(resp.Item2);

            }
            catch (Exception err)
            {
                await _logger.Error(-1, "Failed to convert user to permanent", err);
                return StatusCode(500, err);
            }
        }

        [HttpPost("resetpassword")]
        public async Task<IActionResult> UserResetPasswordRequest([FromBody]UserResetPasswordRequest request)
        {
            /* disabled until can figure out how to send secrets via client when not logged in */
            //if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
            //    return Unauthorized();

            try
            {
                UserService service = new UserService(_authContext, _options, _logger, _smtpOptions);
                var resp = await service.UserResetPasswordRequest(request);

                if (resp.Item1 != null)
                    return Ok(resp.Item1);

                if (resp.Item2.Contains("already exists", StringComparison.CurrentCultureIgnoreCase))
                    return StatusCode((int)HttpStatusCode.Conflict, resp.Item2);

                return BadRequest(resp.Item2);
            }
            catch (Exception ex)
            {
                await _logger.Error(request?.BID ?? -1, "Failed to reset password", ex);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// API end point for resetting a user's password with a newly provided one
        /// </summary>
        /// <param name="resetRequest">A password reset request containing a reset token and new password</param>
        /// <returns>OkResult if successful, BadRequestResult if failure or error</returns>
        [HttpPost("passwordreset")]
        public async Task<IActionResult> ResetPassword([FromBody]PasswordResetRequest request)
        {
            /* disabled until can figure out how to send secrets via client when not logged in */
            //if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
            //    return Unauthorized();

            UserService service = new UserService(_authContext, _options, _logger, _smtpOptions);
            var resp = await service.ResetPassword(request);

            if (resp.Success)
                return new OkObjectResult(resp);

            return BadRequest(resp.Response);
        }

        #region Private Helpers

        private bool SecretMatches(string auth)
        {
            if (auth == null || !auth.StartsWith("Bearer "))
            {
                return false;
            }
            else
            {
                string value = auth.Substring("Bearer ".Length);
                try
                {
                    return Encoding.UTF8.GetString(Convert.FromBase64String(value)) == _secretOptions.TenantSecret;
                }
                catch
                {
                    return false;
                }
            }
        }

        #endregion

    }
}
