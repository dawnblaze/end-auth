﻿using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Auth.Web.Models;
using Endor.Auth.Web.Services;
using Endor.Logging.Client;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Endor.Auth.Web.Controllers
{
    [Route("[controller]")]
    public class TokenController : Controller
    {
        private readonly AuthContext _authContext;
        private readonly EndorAuthOptions _options;
        private readonly RemoteLogger _logger;
        private readonly ITenantDataCache _tenantDataCache;

        public TokenController(AuthContext authContext, IOptions<EndorAuthOptions> options, RemoteLogger logger, ITenantDataCache tenantDataCache)
        {
            _authContext = authContext;
            _options = options.Value;
            _logger = logger;
            _tenantDataCache = tenantDataCache;
        }

        [HttpPost]
        [HttpOptions]
        public async Task<IActionResult> Post([FromBody]PasswordTokenRequest loginRequest)
        {
            await _logger.Information(0, $"Attempting to log {loginRequest.UserName} into {loginRequest.BusinessAlias}.");

            string userAgent = GetUserAgent();

            if (loginRequest.AuthenticationType == 0)
                loginRequest.AuthenticationType = AuthenticationType.Password;

            TokenServiceLoginResult result = await new TokenService(_authContext, _options, _logger).Login(loginRequest, userAgent);
            if (result.Success)
            {
                return new OkObjectResult(result.TokenLoginResponse);
            }

            return new UnauthorizedResult();
        }

        private string GetUserAgent()
        {
            StringValues headers = this.Request.Headers["User-Agent"];
            if (StringValues.IsNullOrEmpty(headers))
                return null;

            return headers[0];
        }

        [HttpPost("form")]
        public async Task<IActionResult> PostForm(PasswordTokenRequest loginRequest)
        {
            string userAgent = GetUserAgent();

            if (loginRequest.AuthenticationType == 0)
                loginRequest.AuthenticationType = AuthenticationType.Password;

            TokenServiceLoginResult result = await new TokenService(_authContext, _options, _logger).Login(loginRequest, userAgent);
            if (result.Success)
            {
                return new OkObjectResult(result.TokenLoginResponse);
            }

            return new UnauthorizedResult();
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh([FromBody]RefreshTokenRequest refreshRequest)
        {
            string userAgent = GetUserAgent();
            TokenServiceRefreshResult result = await new TokenService(_authContext, _options, _logger).Refresh(refreshRequest, userAgent);
            if (result.Success)
            {
                return new OkObjectResult(result.RefreshResponse);
            }

            return new UnauthorizedResult();
        }

        [HttpPost("business")]
        [Authorize]
        public async Task<IActionResult> Business([FromBody]BusinessLoginRequest businessRequest)
        {
            string userAgent = GetUserAgent();
            BusinessLogin result = await new TokenService(_authContext,_options, _logger).GetBusinessLogin(HttpContext.User.Identity, businessRequest.BusinessAlias, userAgent);

            if (result != null)
            {
                return new OkObjectResult(result);
            }
            else
            {
                return new ForbidResult();
            }
        }
    }
}
