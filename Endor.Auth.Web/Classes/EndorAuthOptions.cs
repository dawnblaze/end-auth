﻿namespace Endor.Auth.Web.Classes
{
    public class EndorAuthOptions : Endor.Tenant.ITenantSecretOptions
    {
        public string TenantSecret { get; set; }
        public string TenantUrl { get; set; }
        public double RefreshExpirationDays { get; set; }
    }
}
