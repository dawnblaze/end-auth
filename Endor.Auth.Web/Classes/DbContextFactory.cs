﻿using Endor.Auth.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Classes
{
    public class DbContextFactory : IDesignTimeDbContextFactory<AuthContext>
    {
        public DbContextFactory()
        {

        }

        public AuthContext CreateDbContext(string[] args)
        {
            //    Configuration.GetConnectionString("AuthContext")


            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)

                .AddUserSecrets<Startup>();

            IConfigurationRoot config = configurationBuilder.Build();
            args = new string[] { config.GetConnectionString("AuthContext") };

            string connString = args[0];

            var optionsBuilder = new DbContextOptionsBuilder<AuthContext>();
            Console.WriteLine($"using connection string of: {connString}");
            optionsBuilder.UseSqlServer<AuthContext>(connString);

            return new AuthContext(optionsBuilder.Options);
        }
    }

}
