﻿using Endor.Auth.Models.Entities;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Classes
{
    public class EFTenantDataCache: TenantDataCache
    {
        private readonly AuthContext _authContext;

        public EFTenantDataCache(IMemoryCache cache, AuthContext authContext, int expirationMilliseconds = DefaultExpirationMilliseconds) : base(cache, expirationMilliseconds)
        {
            _authContext = authContext;
        }

        protected override async Task<TenantData> GetTenantData(short bid)
        {
            return await GetTenantDataFromDatabase(this._authContext, bid);
        }

        /// <summary>
        /// Pulls tenant data from a context - this IS NOT from a cache
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static async Task<TenantData> GetTenantDataFromDatabase(AuthContext ctx, short bid)
        {
            Auth.Models.Entities.BusinessData bd = await ctx.BusinessData
                .Include(b => b.BusinessDatabase)
                .Include(b => b.LoggingDatabase)
                .Include(b => b.ReportDatabase)
                .Include(b => b.SystemDatabase)
                .Include(b => b.ServerLinks)
                .ThenInclude(sl => sl.Server)
                .FirstOrDefaultAsync(t => t.BID == bid);

            if (bd == null)
                return null;

            return new TenantData()
            {
                APIURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.API)?.Server.URL,
                ReportingURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.DocumentReports)?.Server.URL,
                MessagingURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.RTC)?.Server.URL,
                LoggingURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Log)?.Server.URL,
                BackgroundEngineURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.BGEngine)?.Server.URL,
                AnalyticsURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.AnalyticReports)?.Server.URL,
                PaymentURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.APE)?.Server.URL,
                SearchURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Search)?.Server.URL,
                StorageConnectionString = bd.StorageConnectionString,
                IndexStorageConnectionString = bd.IndexStorageConnectionString,
                StorageURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Storage)?.Server.URL,
                BusinessDBConnectionString = bd.BusinessDatabase.ConnectionString,
                LoggingDBConnectionString = bd.LoggingDatabase.ConnectionString,
                MessagingDBConnectionString = "",
                SystemReportBConnectionString = bd.ReportDatabase.ConnectionString,
                SystemDataDBConnectionString = bd.SystemDatabase.ConnectionString,
                ExternalTaxEngineURL = bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.ATE)?.Server.URL,
                ServerLinks = bd.ServerLinks.Select(sl => new Tenant.ServerLink()
                {
                    ServerType = (byte)sl.Server.ServerType,
                    URL = sl.Server.URL
                }).ToList()
            };
        }
    }
}
