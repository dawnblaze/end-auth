﻿using Newtonsoft.Json;
using System;
using Endor.Auth.Models.Entities;

namespace Endor.Auth.Web.Models
{
    public interface IPasswordTokenRequest
    {
        string BusinessAlias { get; set; }
        string Password { get; set; }
        string UserName { get; set; }
        Guid? Invite { get; set; }
        AuthenticationType AuthenticationType { get; set; }
    }

    public class PasswordTokenRequest : IPasswordTokenRequest
    {
        [JsonProperty("businessalias")]
        public string BusinessAlias { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("invite")]
        public Guid? Invite { get; set; }
        [JsonProperty("authenticationtype")]
        public AuthenticationType AuthenticationType { get; set; }
    }

    public class PasswordTokenFormRequest : IPasswordTokenRequest
    {
        public string BusinessAlias { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public Guid? Invite { get; set; }
        public AuthenticationType AuthenticationType { get; set; }
    }

}
