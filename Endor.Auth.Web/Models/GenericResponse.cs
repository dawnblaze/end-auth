﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    /// <summary>
    /// A generic Response object
    /// </summary>
    public class GenericResponse
    {
        /// <summary>
        /// Response message
        /// </summary>
        public string Response { get; internal set; }

        /// <summary>
        /// Whether or not the action was successful
        /// </summary>
        public bool Success { get; internal set; }
    }
}
