﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    /// <summary>
    /// User Revoke Access Request
    /// </summary>
    public class UserRevokeAccessRequest
    {
        [JsonProperty("authuserid")]
        public int UserID { get; set; }
        [JsonProperty("bid")]
        public short BID { get; set; }
    }
}
