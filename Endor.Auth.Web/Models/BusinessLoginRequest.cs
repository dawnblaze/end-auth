﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    public class BusinessLoginRequest
    {
        [JsonProperty("businessalias")]
        public string BusinessAlias { get; set; }
    }
}
