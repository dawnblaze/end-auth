﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    /// <summary>
    /// Revoke Invite Request
    /// </summary>
    public class RevokeInviteRequest
    {
        /// <summary>
        /// Invite Guid
        /// </summary>
        [JsonProperty("invite")]
        public Guid InviteGUID { get; set; }
    }
}
