﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    /// <summary>
    /// Create Business Response
    /// </summary>
    public class CreateBusinessResponse : GenericResponse
    {
        /// <summary>
        /// Business Id
        /// </summary>
        public short BID { get; set; }
    }
}
