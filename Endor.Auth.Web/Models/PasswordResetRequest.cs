﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    /// <summary>
    /// A standard password reset request
    /// </summary>
    public class PasswordResetRequest
    {
        /// <summary>
        /// Business Id
        /// </summary>
        public short? BID { get; set; }

        /// <summary>
        /// Business alias
        /// </summary>
        public string BusinessAlias { get; set; }

        /// <summary>
        /// User's email address
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Password reset token
        /// </summary>
        public string ResetToken { get; set; }

        /// <summary>
        /// New password
        /// </summary>
        public string NewPassword { get; set; }
    }
}
