﻿using Endor.Auth.Models.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    public class AuthResponse
    {
        [JsonProperty("user")]
        public BusinessUser User { get; set; }
        [JsonProperty("businesslogin")]
        public BusinessLogin BusinessLogin { get; set; }
        [JsonProperty("language")]
        public UserBusinessLanguage Language { get; set; }
    }

    public class BusinessUser
    {
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("userID")]
        public int UserID { get; set; }
        [JsonIgnore]
        public UserData UserData { get; set; }
    }

    public class BusinessLoginUrls
    {
        [JsonProperty("api")]
        public string Api { get; set; }
        [JsonProperty("logging")]
        public string Logging { get; set; }
        [JsonProperty("messaging")]
        public string Messaging { get; set; }
        [JsonProperty("searchindex")]
        public string SearchIndex { get; set; }
        [JsonProperty("search")]
        public string Search { get; internal set; }
        [JsonProperty("document")]
        public string Document { get; internal set; }
        [JsonProperty("board")]
        public string Board { get; internal set; }
        [JsonProperty("report")]
        public string Report { get; internal set; }
        [JsonProperty("analytics")]
        public string Analytics { get; internal set; }
        [JsonProperty("payment")]
        public string Payment { get; internal set; }
        [JsonProperty("externaltaxserver")]
        public string ExternalTaxServer { get; internal set; }
        [JsonProperty("serverlinks")]
        public List<Tenant.ServerLink> ServerLinks { get; set; }

    }

    public class BusinessLogin
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("BID")]
        public short BID { get; set; }
        [JsonProperty("url")]
        public BusinessLoginUrls url { get; set; }
        [JsonProperty("rights")]
        public string Rights { get; set; }
        public int ExpiresIn { get; internal set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("nonce")]
        public Guid Nonce { get; set; }
        [JsonProperty("sas_token")]
        public string SASToken { get; set; }
    }

    public class UserBusinessLanguage
    {
        [JsonProperty("language_code")]
        public string languageCode { get; set; }
        [JsonProperty("google_code")]
        public string GoogleCode { get; set; }
    }
}
