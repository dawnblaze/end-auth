﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    /// <summary>
    /// Generate Invite Request Response
    /// </summary>
    public class GenerateInviteRequestResponse : GenerateInviteRequest, IGenerateEmailResponse
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid ID { get; set; }
    }
}
