﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    public class TokenServiceRefreshResult
    {
        public bool Success { get; internal set; }
        public AuthResponse RefreshResponse { get; internal set; }
    }
}
