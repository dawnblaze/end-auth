﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    public interface IGenerateEmailResponse
    {
        /// <summary>
        /// Business Id
        /// </summary>
        short BID { get; set; }

        /// <summary>
        /// Unique ID for the Request
        /// </summary>
        Guid ID { get; set; }

        /// <summary>
        /// Suggested Email
        /// </summary>
        string SuggestedEmail { get; set; }

        /// <summary>
        /// User's Display Name
        /// </summary>
        string DisplayName { get; set; }

        /// <summary>
        /// Property Indicating if Employee Location has an image associated with it
        /// </summary>
        bool HasImage { get; set; }

        /// <summary>
        /// Blob Uri of Image if one exists
        /// </summary>
        string ImageLocation { get; set; }
    }
}
