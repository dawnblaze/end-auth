﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Models
{
    public class UserResetPasswordRequest
    {
        [JsonProperty("bid")]
        public short BID { get; set; }

        [JsonProperty("businessalias")]
        public string BusinessAlias { get; set; }

        [JsonProperty("userid")]
        public int UserID { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }
}
