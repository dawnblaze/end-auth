USE [Dev.Endor.Master]
GO

/****** Object:  StoredProcedure [dbo].[Util.Business.Delete]    Script Date: 03/04/19 7:55:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Util.Business.Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [dbo].[Util.Business.Delete]
END
GO

CREATE PROCEDURE [dbo].[Util.Business.Delete]( @BID SMALLINT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteBusiness]
    BEGIN TRY

		DELETE FROM [Client.Data] WHERE BID = @BID
		DELETE FROM [User.RefreshToken] WHERE BID = @BID
		DELETE FROM [User.Invite] WHERE BID = @BID
		
		DELETE FROM [Business.CustomURL] WHERE BID = @BID
		DELETE FROM [User.BusinessLink] WHERE BID = @BID

		DELETE FROM [User.Cred]
		WHERE UserID NOT IN (SELECT X.UserID FROM [User.BusinessLink] X)

		DELETE FROM [User.Data]
		WHERE ID NOT IN (SELECT X.UserID FROM [User.BusinessLink] X) 
		AND ID NOT IN (SELECT X.UserID FROM [Client.Data] X)

		DELETE FROM [Client.Device]
		WHERE ID NOT IN (SELECT X.DeviceID FROM [Client.Data] X)
		
		DELETE FROM [Business.Data] WHERE BID = @BID
		DELETE FROM [Business.Association] 
		WHERE ID NOT IN (SELECT X.AssociationID FROM [Business.Data] X)

		DELETE FROM [Database.Data]
		WHERE ID NOT IN (SELECT X.BusinessDatabaseID FROM [Business.Data] X)
		AND ID NOT IN (SELECT X.SystemDatabaseID FROM [Business.Data] X)
		AND ID NOT IN (SELECT X.ReportDatabaseID FROM [Business.Data] X)
		AND ID NOT IN (SELECT X.LoggingDatabaseID FROM [Business.Data] X)
		
        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteBusiness]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteBusiness];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [DeleteBusiness]

END;

GO


