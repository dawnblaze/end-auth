IF EXISTS(SELECT * FROM sys.objects WHERE type = 'IF' AND name = 'User.Security.Rights.List')
  DROP FUNCTION [User.Security.Rights.List];
GO
-- ========================================================
-- 
-- Name: User.Security.Rights.List( BID int, UserID int )
--
-- Description: This Table Value Function returns a list of Rights associated
-- with a specific user for a given business id.
--
-- Sample Use:   select * from [User.Security.Rights.List] ( 1, 100 )
--
-- ========================================================
CREATE FUNCTION [User.Security.Rights.List] ( @BID int, @UserID int )
RETURNS TABLE
AS
RETURN
(
    WITH Rights(BID, GroupID)
    AS
    (
        -- Anchor Query

        -- Pull the users ROLE security group
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Role.Link] SRoL on SRoL.BID = U.BID AND SRoL.RoleType = U.RoleType
        JOIN [Security.Right.Group] SRG on SRG.BID = SRoL.BID AND SRG.ID = SRoL.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Merge with the users top-level security group
        UNION ALL 
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Right.Group] SRG on SRG.BID = U.BID AND SRG.ID = U.RightGroupID
        LEFT JOIN [Security.Right.Link] SRiL on SRiL.BID = SRG.BID AND SRiL.RightGroupID = SRG.ID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Now merge with recursively with Security Groups
        UNION ALL
        SELECT G.BID, G.ID
        FROM Rights R 
        JOIN [Security.Right.Group.Link] L ON L.BID = R.BID AND L.ParentGroupID = R.GroupID
        JOIN [Security.Right.Group] G ON G.BID = L.BID AND G.ID = L.ChildGroupID
    )
        SELECT DISTINCT RL.BID, RL.RightID 
        FROM Rights R
        JOIN [Security.Right.Link] RL on RL.BID = R.BID and RL.RightGroupID = R.GroupID
);