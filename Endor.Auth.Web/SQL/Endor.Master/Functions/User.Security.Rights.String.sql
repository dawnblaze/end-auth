IF EXISTS(SELECT * FROM sys.objects WHERE type = 'FN' AND name = 'User.Security.Rights.String')
  DROP FUNCTION [User.Security.Rights.String];
GO
-- ========================================================
-- 
-- Name: User.Security.Rights.String( BID int, UserID int )
--
-- Description: This Table Value Function returns a list of Rights associated
-- with a specific user for a given business id.
--
-- Sample Use:   select dbo.[User.Security.Rights.String]( 1, 100 )
--
-- ========================================================
CREATE FUNCTION [User.Security.Rights.String] ( @BID int, @UserID int )
RETURNS BINARY(128)
AS
BEGIN
    DECLARE @ResultSize SMALLINT = 128
	      , @Found BIT = 0
          , @Results BINARY(128) = 0; 

    DECLARE @Temp Table (RightID smallint, ByteIndex tinyint, BytePos tinyint, ByteFlag tinyint);

    WITH Rights(BID, GroupID)
    AS
    (
        -- Anchor Query

        -- Pull the users ROLE security group
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Role.Link] SRoL on SRoL.BID = U.BID AND SRoL.RoleType = U.RoleType
        JOIN [Security.Right.Group] SRG on SRG.BID = SRoL.BID AND SRG.ID = SRoL.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Merge with the users top-level security group
        UNION ALL 
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Right.Group] SRG on SRG.BID = U.BID AND SRG.ID = U.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Now merge with recursively with Security Groups
        UNION ALL
        SELECT G.BID, G.ID
        FROM Rights R 
        JOIN [Security.Right.Group.Link] L ON L.BID = R.BID AND L.ParentGroupID = R.GroupID
        JOIN [Security.Right.Group] G ON G.BID = L.BID AND G.ID = L.ChildGroupID
    )
        SELECT @Results = SUBSTRING(@Results, 1, @ResultSize-ByteIndex) + CONVERT(BINARY(1),CHAR(Flags)) + SUBSTRING(@Results, @ResultSize-(ByteIndex-2), ByteIndex-1)
		     , @Found = 1  -- use this to determine that we really found results.  Will not get set so @Found will remain 0 if not
        FROM (
            -- Sum the Byte Flag by ByteIndex to get the character to insert
            SELECT ByteIndex, SUM(ByteFlag) Flags
            FROM (
                SELECT DISTINCT 
                      (RL.RightID / 8)+1 as ByteIndex
                    , Power(2, RL.RightID % 8) as ByteFlag
                    --, (RL.RightID % 8) as BytePos
                    -- , RL.RightID
                FROM Rights R
                JOIN [Security.Right.Link] RL on RL.BID = R.BID and RL.RightGroupID = R.GroupID
            ) RightsList
            GROUP By ByteIndex
        ) RightsString
        ;

		IF (@Found=0) SET @Results = NULL;

        RETURN @Results;
END;