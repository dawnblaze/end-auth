﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Auth.Web.Models;
using Endor.Configuration;
using Endor.Logging.Client;
using Endor.Tenant;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Options;

namespace Endor.Auth.Web.Services
{
    /// <summary>
    /// Email Service used to connect to SMTP Server
    /// </summary>
    public class SMTPEmailService
    {
        private const string LocationImagePartialTemplateFileName = "location_image.partial.html";
        private const string CreateUserAccountTemplateFileName = "create_user_account.html";

        /// <summary>
        /// Remote logger
        /// </summary>
        private readonly RemoteLogger _logger;
        /// <summary>
        /// SMTP Options for sending emails
        /// </summary>
        private readonly IOptions<SMTPOptions> _smtpOptions;
        /// <summary>
        /// SMTP Client
        /// </summary>
        private readonly SmtpClient _client;

        /// <summary>
        /// Enum used to indicate which HTML template is to be used
        /// </summary>
        private enum HtmlTemplate
        {
            //Full Templates
            UserInvite,
            ExistingUserInvite,
            UserAccountNotFound,
            UserPasswordReset,

            //Partials
            LocationImagePartial,
        }

        /// <summary>
        /// Account Service used to connect to the DBContext
        /// </summary>
        /// <param name="options">Authorization options</param>
        /// <param name="logger">Remote logger</param>
        public SMTPEmailService(IOptions<SMTPOptions> smtpOptions, RemoteLogger logger)
        {
            _smtpOptions = smtpOptions;
            _logger = logger;
            _client = new SmtpClient(_smtpOptions.Value.Host, _smtpOptions.Value.Port)
            {
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_smtpOptions.Value.UserName, _smtpOptions.Value.Password)
            };
        }

        /// <summary>
        /// Trigger the message to be sent
        /// </summary>
        public async Task SendMailAsync(MailMessage message)
        {
            await _client.SendMailAsync(message);
        }

        /// <summary>
        /// Send Reset Password Email
        /// </summary>
        public async Task SendResetEmail(BusinessData business, GeneratePasswordResetResponse response)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress(_smtpOptions.Value.MailFromAddress)
            };

            var template = GetHtmlTemplate(HtmlTemplate.UserPasswordReset, business, response);
            mailMessage.To.Add(response.SuggestedEmail);
            mailMessage.Body = template;
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = "Password Reset Request";
            await SendMailAsync(mailMessage);
        }

        /// <summary>
        /// Send User Invite Email
        /// </summary>
        public async Task SendUserInviteEmail(BusinessData business, GenerateInviteRequestResponse response)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress(_smtpOptions.Value.MailFromAddress, "CoreBridge Automated Email")
            };

            var template = GetHtmlTemplate(HtmlTemplate.UserInvite, business, response);
            mailMessage.To.Add(response.SuggestedEmail);
            mailMessage.Body = template;
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = "You are invited";
            await SendMailAsync(mailMessage);
        }

        public async Task SendExistingUserInviteEmail(BusinessData business, GenerateInviteRequestResponse response)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress(_smtpOptions.Value.MailFromAddress, "CoreBridge Automated Email")
            };

            var template = GetHtmlTemplate(HtmlTemplate.ExistingUserInvite, business, response);
            mailMessage.To.Add(response.SuggestedEmail);
            mailMessage.Body = template;
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = "You are invited";
            await SendMailAsync(mailMessage);
        }

        private string GetHtmlTemplate(HtmlTemplate template, BusinessData business, IGenerateEmailResponse request)
        {
            var directory = Path.Combine(Directory.GetCurrentDirectory(), "EmailTemplates");

            string
                mainTemplatePath = null,
                mainTemplateBody = null,
                locationImagePartialPath = null,
                locationImagePartialBody = null;

            switch (template)
            {
                case HtmlTemplate.UserInvite:
                case HtmlTemplate.ExistingUserInvite:
                    mainTemplatePath = Path.Combine(directory, CreateUserAccountTemplateFileName);
                    locationImagePartialPath = Path.Combine(directory, "partials", SMTPEmailService.LocationImagePartialTemplateFileName);
                    break;
                case HtmlTemplate.UserAccountNotFound:
                    mainTemplatePath = Path.Combine(directory, "user_account_not_found.html");
                    break;
                case HtmlTemplate.UserPasswordReset:
                    mainTemplatePath = Path.Combine(directory, "reset_password.html");
                    locationImagePartialPath = Path.Combine(directory, "partials", SMTPEmailService.LocationImagePartialTemplateFileName);
                    break;
                default:
                    throw new ArgumentException("Unsupported HTML Template Specified");
            }

            //Get main HTML template
            try
            {
                if (mainTemplatePath != null)
                    mainTemplateBody = File.ReadAllText(mainTemplatePath);

                if (locationImagePartialPath != null)
                    locationImagePartialBody = File.ReadAllText(locationImagePartialPath);
            }
            catch
            {
                throw new Exception("Failed to load the specified HTML template");
            }

            //Main HTML template cannot be empty
            if (string.IsNullOrWhiteSpace(mainTemplateBody))
                throw new Exception("HTML template was found by is empty");

            string firstName = request.DisplayName ?? "";
            if (firstName.Length > 0 && firstName.IndexOf(' ') != -1)
                firstName = firstName.Substring(0, firstName.IndexOf(' '));

            string encodedEmail = HttpUtility.UrlEncode(request.SuggestedEmail);
            mainTemplateBody = mainTemplateBody.Replace("{company.name}", business.CompanyName ?? "");
            mainTemplateBody = mainTemplateBody.Replace("{first.name}", firstName);
            mainTemplateBody = mainTemplateBody.Replace("{email.address}", request.SuggestedEmail ?? "");
            mainTemplateBody = mainTemplateBody.Replace("{try.again.link}", $@"{business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Web).Server.URL}/u/0/reset-password?b={business.BusinessAppName}");
            mainTemplateBody = mainTemplateBody.Replace("{create.user.account.link}", $@"{business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Web).Server.URL}/{(template != HtmlTemplate.ExistingUserInvite?"new-user":"link-user")}?b={business.BusinessAppName}&e={encodedEmail}&t={request.ID}");
            mainTemplateBody = mainTemplateBody.Replace("{reset.user.password.link}", $"{business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Web).Server.URL}/reset-password?b={business.BID}&resetToken={request.ID}");
            
            //Merge partial template(s) if available
            if (!string.IsNullOrWhiteSpace(locationImagePartialBody))
            {
                //Handling for Employee's Location Image
                if (!request.HasImage)
                {
                    //No image available - replace merge field for partial template with an empty string
                    mainTemplateBody = mainTemplateBody.Replace("{location.image.partial}", string.Empty);
                }
                else
                {
                    //Image exists - Merge fields into partial template
                    locationImagePartialBody = locationImagePartialBody.Replace("{comapny.name}", business.CompanyName ?? "");
                    locationImagePartialBody = locationImagePartialBody.Replace("{logo.img.src}", request.ImageLocation);

                    //Finally merge partial into main template
                    mainTemplateBody = mainTemplateBody.Replace("{location.image.partial}", locationImagePartialBody);
                }
            }

            return mainTemplateBody;
        }
    }
}
