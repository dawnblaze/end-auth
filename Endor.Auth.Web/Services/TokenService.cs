﻿using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Auth.Web.Middleware;
using Endor.Auth.Web.Models;
using Endor.AzureStorage;
using Endor.Logging.Client;
using Endor.Tenant;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Endor.Auth.Web.Services
{
    public class TokenService
    {
        private enum AudienceType {Server, Storage, CustomUrl};
        private class AudienceItem
        {
            public string URL { get; set; }
            public AudienceType Type { get; set; }
        }
        private class AudienceList : List<AudienceItem> { }

        private readonly AuthContext _authContext;
        private readonly EndorAuthOptions _options;
        private readonly RemoteLogger _logger;

        public TokenService(AuthContext authContext, EndorAuthOptions options, RemoteLogger logger)
        {
            _authContext = authContext;
            _options = options;
            _logger = logger;
        }

        public static TokenProviderOptions Options { get; internal set; }

        private static Dictionary<short, AudienceList> _Audiences;

        public static void RemoveAudience(short bid)
        {
            if (_Audiences.ContainsKey(bid))
                _Audiences.Remove(bid);
        }

        public static void AddAudience(BusinessData business)
        {
            if (business != null)
            {
                AudienceList audienceList = GetAudienceList(business);

                if (audienceList != null)
                    _Audiences[business.BID] = audienceList;
            }
        }

        private async Task ReloadAudience(BusinessData business)
        {
            if (_Audiences.TryGetValue(business.BID, out AudienceList currentBusinessAudiences))
            {
                AudienceList newBusinessAudiences = GetAudienceList(business);

                List<string> servers = currentBusinessAudiences.Where(x => x.Type == AudienceType.Server)
                                                               .Select(x => x.URL)
                                                               .ToList();

                if (newBusinessAudiences == null)
                    RemoveAudience(business.BID);
                else
                {
                    newBusinessAudiences.ForEach(x =>
                    {
                        if (x.Type == AudienceType.Server && !servers.Contains(x.URL))
                            servers.Add(x.URL);
                    });

                    _Audiences[business.BID] = newBusinessAudiences;
                }

                foreach (string serverURL in servers)
                {
                    using (var client = new HttpClient())
                    {
                        using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, $"{serverURL}api/cache/invalidate?bid={business.BID}"))
                        {
                            try
                            {
                                HttpResponseMessage response = await client.SendAsync(request);

                                if (response.IsSuccessStatusCode)
                                    await _logger.Information(business.BID, $"Cache invalidated at {serverURL}");

                                else
                                {
                                    string responseString = await response.Content.ReadAsStringAsync();
                                    Exception ex = new Exception($"HTTP Response of {(int)response.StatusCode} {response.StatusCode}.  {responseString}");
                                    await _logger.Error(business.BID, $"Error invalidating cache at {serverURL}", ex);
                                }
                            }
                            catch (Exception ex)
                            {
                                await _logger.Error(business.BID, $"Error invalidating cache at {serverURL}", ex);
                            }
                        }
                    }
                }
            }
            else
                AddAudience(business);
        }

        public async Task ReloadAudiences(short? bid = null)
        {
            if (bid.HasValue)
            {
                BusinessData business = await _authContext.GetBusinessByBIDAsync(bid.Value);
                await ReloadAudience(business);
            }
            else
            {
                List<BusinessData> businesses = await _authContext.GetAllBusinessesAsync();

                foreach (BusinessData business in businesses)
                    await ReloadAudience(business);
            }
        }

        internal static void FillAudiences(AuthContext authContext)
        {
            Dictionary<short, AudienceList> newAudiences = new Dictionary<short, AudienceList>();

            List<BusinessData> businesses = authContext.GetAllBusinesses();

            foreach (BusinessData newBusiness in businesses)
                newAudiences[newBusiness.BID] = GetAudienceList(newBusiness);

            _Audiences = newAudiences;
        }

        private AudienceList GetAudiences(short bid)
        {
            if (_Audiences.TryGetValue(bid, out AudienceList audiences))
                return audiences;

            return null;
        }

        private static AudienceList GetAudienceList(BusinessData business)
        {
            if (business == null)
                return null;

            AudienceList businessAudiences = new AudienceList
            {
                new AudienceItem { URL = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.API)?.Server.URL, Type = AudienceType.Server},
                new AudienceItem { URL = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Storage)?.Server.URL, Type = AudienceType.Storage},
                new AudienceItem { URL = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Log)?.Server.URL, Type = AudienceType.Server},
                new AudienceItem { URL = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.RTC)?.Server.URL, Type = AudienceType.Server},
                new AudienceItem { URL = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Search)?.Server.URL, Type = AudienceType.Server},
                new AudienceItem { URL = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Board)?.Server.URL, Type = AudienceType.Server},
                new AudienceItem { URL = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.DocumentReports)?.Server.URL, Type = AudienceType.Server},
                new AudienceItem { URL = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.MessageEmail)?.Server.URL, Type = AudienceType.Server}
            };

            if (business.BusinessCustomUrl != null)
                foreach (var custUrl in business.BusinessCustomUrl)
                    businessAudiences.Add(new AudienceItem { URL = custUrl.CustomUrl, Type = AudienceType.CustomUrl });

            return businessAudiences;
        }

        public async Task<TokenServiceLoginResult> Login(IPasswordTokenRequest value, string deviceID = null)
        {
            try
            {
                AuthResponse loginResponse = await GenerateLoginResponse(value, deviceID);
                if (loginResponse != null && loginResponse.BusinessLogin != null)
                {
                    await this._logger.Billing(Logging.Models.BillingEntryType.Login, loginResponse.BusinessLogin.BID, "", loginResponse.User.UserID, "User logged in");
                    return new TokenServiceLoginResult() { Success = true, TokenLoginResponse = loginResponse };
                }
            }
            catch (Exception ex)
            {
                await _logger.Error(0, "Error at GenerateLoginResponse", ex);
            }

            return new TokenServiceLoginResult() { Success = false };
        }

        public async Task<TokenServiceRefreshResult> Refresh(RefreshTokenRequest refreshRequest, string deviceID = null)
        {
            try
            {
                AuthResponse loginResponse = await GenerateLoginResponse(refreshRequest, deviceID);
                if (loginResponse != null)
                {
                    return new TokenServiceRefreshResult() { Success = true, RefreshResponse = loginResponse };
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            return new TokenServiceRefreshResult() { Success = false };
        }

        /// <summary>
        /// Given a pre-existing identity, logs in to a business w/ same username/userID (but not same rights!)
        /// </summary>
        /// <param name="existingIdentity"></param>
        /// <param name="businessAlias"></param>
        /// <returns></returns>
        public async Task<BusinessLogin> GetBusinessLogin(IIdentity existingIdentity, string businessAlias, string deviceID = null)
        {
            BusinessData bd = await _authContext.GetBusinessByAliasAsync(businessAlias);

            if (bd != null && existingIdentity is ClaimsIdentity)
            {
                Claim userIDClaim = (existingIdentity as ClaimsIdentity).FindFirst(ClaimNameConstants.UserID);
                if (userIDClaim is null)
                {
                    return null;
                }
                int userID = int.Parse(userIDClaim.Value);

                var rightsAndIDs = await GetRightsAndIDs(bd, userID, null);
                string accessType = rightsAndIDs?.accessType;
                string rights = rightsAndIDs?.rights;

                Claim accessTypeClaim = new Claim(ClaimNameConstants.AccessType, accessType);
                Claim rightsClaim = new Claim(ClaimNameConstants.Rights, rights);

                return await GetBusinessLogin(existingIdentity as ClaimsIdentity, bd, accessTypeClaim, rightsClaim, deviceID);
            }
            else
            {
                return null;
            }
        }

        private async Task<AuthResponse> GenerateLoginResponse(IPasswordTokenRequest value, string deviceID)
        {
            try
            {
                BusinessUser user = await GetUser(value.UserName, value.Password, value.AuthenticationType, value.Invite);
                if (user == null)
                {
                    return null;
                }

                BusinessLogin businessLogin = await GetBusinessLogin(user, value.BusinessAlias, deviceID);
                if (businessLogin == null)
                    return null;

                UserBusinessLanguage userLanguage = await GetUserBusinessLanguage(user, businessLogin);
                if (userLanguage == null)
                    return null;

                return new AuthResponse() { User = user, BusinessLogin = businessLogin, Language = userLanguage };
            }
            catch (Exception ex)
            {
                await _logger.Error(0, "Error in GenerateLoginResponse", ex);
                return null;
            }
        }

        private async Task<AuthResponse> GenerateLoginResponse(RefreshTokenRequest refreshRequest, string deviceID)
        {
            BusinessUser user = await GetUser(refreshRequest.RefreshToken, refreshRequest.Nonce);
            if (user == null)
                return null;

            BusinessLogin businessLogin = await GetBusinessLogin(user, refreshRequest, deviceID);
            if (businessLogin == null)
                return null;
            UserBusinessLanguage userLanguage = await GetUserBusinessLanguage(user, businessLogin);
            if (userLanguage == null)
                return null;
            return new AuthResponse() { User = user, BusinessLogin = businessLogin, Language = userLanguage };
        }

        private async Task<UserBusinessLanguage> GetUserBusinessLanguage(BusinessUser user, BusinessLogin business)
        {
            int languageOptionNumber = 1045;
            var client = Http.Client;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", business.AccessToken);
            var getLanguageResponse = client.GetAsync($"{business.url.Api}api/options/{languageOptionNumber}?userLinkID={user.UserID}");
            var availableLanguagesResponse = client.GetAsync($"{business.url.Api}api/system/translate/languages");
            if ((await getLanguageResponse).IsSuccessStatusCode && (await availableLanguagesResponse).IsSuccessStatusCode)
            {
                var languages =  JsonConvert.DeserializeObject<List<SystemLanguageType>>(await (await availableLanguagesResponse).Content.ReadAsStringAsync());
                var optionValue = JsonConvert.DeserializeObject<OptionResponse>(await (await getLanguageResponse).Content.ReadAsStringAsync());
                var languageCode = optionValue.Value.Value;
                return new UserBusinessLanguage
                {
                    languageCode = languageCode,
                    GoogleCode = languages.Where(l => l.Code == languageCode).First().GoogleCode
                };
            }

            return new UserBusinessLanguage
            {
                languageCode = "en",
                GoogleCode = "en"
            };
        }

        private async Task<BusinessLogin> GetBusinessLogin(BusinessUser user, RefreshTokenRequest refreshRequest, string deviceID)
        {
            BusinessData bd = await _authContext.GetBusinessByBIDAsync(refreshRequest.BID);

            if (bd != null)
            {
                ClaimsIdentity identity = await GetNewIdentity(user, bd);

                Claim accessTypeClaim = identity?.FindFirst(ClaimNameConstants.AccessType);
                Claim rightsClaim = identity?.FindFirst(ClaimNameConstants.Rights);

                return await GetBusinessLogin(identity, bd, accessTypeClaim, rightsClaim, deviceID, refreshRequest.Nonce);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Given login info, creates an identity and gets a BusinessLogin
        /// </summary>
        /// <param name="user"></param>
        /// <param name="businessAlias"></param>
        /// <returns></returns>
        private async Task<BusinessLogin> GetBusinessLogin(BusinessUser user, string businessAlias, string deviceID)
        {
            BusinessData business = await _authContext.GetBusinessByAliasAsync(businessAlias);
            if (business != null)
            {
                if (!HasAssociatedBusinessLinks(user.UserData, business))
                    return null;

                ClaimsIdentity identity = await GetNewIdentity(user, business);

                //get the rights we just made
                Claim accessTypeClaim = identity?.FindFirst(ClaimNameConstants.AccessType);
                Claim rightsClaim = identity?.FindFirst(ClaimNameConstants.Rights);

                return await GetBusinessLogin(identity, business, accessTypeClaim, rightsClaim, deviceID);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Given an identity (username and userID) and a bid, and the rights claim, make a JWT token and metadata
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="bid"></param>
        /// <param name="rightsClaim"></param>
        /// <returns>a BusinessLogin, or null if necessary information is missing</returns>
        private async Task<BusinessLogin> GetBusinessLogin(ClaimsIdentity identity, BusinessData business, Claim accessTypeClaim, Claim rightsClaim, string deviceID = null, Guid? nonce = null)
        {
            if (identity == null || rightsClaim == null)
                return null;

            Claim bidClaim = identity.FindFirst(ClaimNameConstants.BID);
            if (bidClaim is null)
                return null;

            Claim userIDClaim = identity.FindFirst(ClaimNameConstants.UserID);
            if (userIDClaim is null)
                return null;

            int userID = int.Parse(userIDClaim.Value);

            Claim userLinkIDClaim = identity.FindFirst(ClaimNameConstants.UserLinkID);
            if (userLinkIDClaim is null)
                return null;

            int userLinkID = int.Parse(userLinkIDClaim.Value);

            Claim employeeIDClaim = identity.FindFirst(ClaimNameConstants.EmployeeID);
            short? employeeID = null;
            if (employeeIDClaim != null)
            {
                employeeID = short.Parse(employeeIDClaim.Value);
            }

            return await GetBusinessLogin(identity.Name, userID, userLinkID, business, accessTypeClaim, rightsClaim, employeeID, deviceID, nonce);
        }

        /// <summary>
        /// Creates a JWT and metadata given primitives and a rights claim. Does not validate input
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userID"></param>
        /// <param name="bid"></param>
        /// <param name="rightsClaim"></param>
        /// <returns></returns>
        private async Task<BusinessLogin> GetBusinessLogin(string userName, int userID, int userLinkID, BusinessData business, Claim accessTypeClaim, Claim rightsClaim, short? employeeID = null, string deviceID = null, Guid? nonce = null)
        {
            DateTime now = DateTime.UtcNow;

            AudienceList validAudiences = GetAudiences(business.BID);

            if (validAudiences == null)
                return null;

            // Specifically add the jti (random nonce), iat (issued timestamp), and sub (subject/user) claims.
            // You can add other claims here, if you want:
            List<Claim> claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(now,TimeSpan.Zero).ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64),
                new Claim(ClaimNameConstants.UserName, userName, ClaimValueTypes.String),
                new Claim(ClaimNameConstants.BID, business.BID.ToString(), ClaimValueTypes.Integer),
                new Claim(ClaimNameConstants.UserLinkID, userLinkID.ToString(), ClaimValueTypes.Integer),
                new Claim(ClaimNameConstants.UserID, userID.ToString(), ClaimValueTypes.Integer),
                new Claim(ClaimNameConstants.AID, business.AssociationID.ToString(), ClaimValueTypes.Integer),

                accessTypeClaim,
                //rightsClaim,
            };

            if (employeeID.HasValue)
                claims.Add(new Claim(ClaimNameConstants.EmployeeID, employeeID.Value.ToString(), ClaimValueTypes.Integer));

            foreach (AudienceItem audience in validAudiences)
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.Aud, audience.URL));
            }

            // Create the JWT and write it to a string
            JwtSecurityToken jwt = new JwtSecurityToken(
                issuer: Options.Issuer,
                claims: claims,
                notBefore: now,
                expires: now.Add(Options.Expiration),
                signingCredentials: Options.SigningCredentials);

            string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            ClientData clientData = await GetClientData(userID, business.BID, deviceID);

            RefreshToken refreshToken = await GetRefreshToken(userID, business.BID, nonce, deviceID);
            BusinessLoginUrls urls = new BusinessLoginUrls()
            {
                Api = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.API)?.Server.URL,
                Logging = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Log)?.Server.URL,
                Messaging = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.RTC)?.Server.URL,
                Search = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Search)?.Server.URL,
                Document = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Storage)?.Server.URL,
                Board = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.Board)?.Server.URL,
                Report = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.DocumentReports)?.Server.URL,
                Analytics = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.AnalyticReports)?.Server.URL,
                Payment = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.APE)?.Server.URL,
                ExternalTaxServer = business.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.ATE)?.Server.URL,
                ServerLinks = business.ServerLinks.Select(sl => new Tenant.ServerLink()
                {
                    ServerType = (byte)sl.Server.ServerType,
                    URL = sl.Server.URL
                }).ToList()
            };

            string storageToken = await GetStorageToken(business.StorageConnectionString, business.BID, now);

            BusinessLogin businessLogin = new BusinessLogin()
            {
                AccessToken = encodedJwt,
                ExpiresIn = (int)Options.Expiration.TotalSeconds,
                Rights = rightsClaim.Value,
                BID = business.BID,
                url = urls,
                RefreshToken = refreshToken.Token,
                Nonce = refreshToken.Nonce,
                SASToken = storageToken,
            };

            return businessLogin;
        }

        private async Task<string> GetStorageToken(string connectionString, short bid, DateTime now)
        {
            try
            {
                return await new EntityStorageClient(connectionString, bid).GetSASTokenAsync();
            }
            catch(Exception e)
            {
                await _logger.Error(bid, "Error connecting to blob storage", e);
                return "";
            }
        }

        private async Task<ClientData> GetClientData(int userID, short bid, string deviceID = null)
        {
            try
            {
                ClientData cd = _authContext.ClientData.Include(t => t.Device).FirstOrDefault(t => t.UserID == userID && t.BID == bid);
                if (cd != null)
                {
                    cd.ClientID = Guid.NewGuid().ToString();
                    cd.LastActivityDT = DateTime.UtcNow;
                    cd.ConnectDT = DateTime.UtcNow;
                }
                else
                {
                    cd = new ClientData()
                    {
                        BID = bid,
                        ClientID = Guid.NewGuid().ToString(),
                        ConnectDT = DateTime.UtcNow,
                        LastActivityDT = DateTime.UtcNow,
                        UserID = userID,
                    };

                    _authContext.ClientData.Add(cd);
                }

                await _authContext.SaveChangesAsync();

                return cd;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return null;
        }

        private async Task<RefreshToken> GetRefreshToken(int userID, short bid, Guid? nonce, string deviceID)
        {
            try
            {
                IEnumerable<RefreshToken> foundTokens = _authContext.RefreshToken
                    .Where(t => t.UserID == userID && t.BID == bid && t.Nonce == nonce && t.RevokedDT == null && t.ExpiresDT < DateTime.UtcNow);

                RefreshToken rt = foundTokens.FirstOrDefault(t => t.ClientID.ToLower() == deviceID.ToLower());
                if (rt != null)
                {
                    rt = await UpdateToken(rt);
                }
                else
                {
                    byte[] randomNumber = new byte[32];
                    System.Security.Cryptography.RandomNumberGenerator.Create().GetBytes(randomNumber);
                    DateTime currentDateTime = DateTime.UtcNow;
                    rt = new RefreshToken()
                    {
                        Token = Base64UrlTextEncoder.Encode(randomNumber),
                        Nonce = Guid.NewGuid(),
                        IssuedDT = currentDateTime,
                        ExpiresDT = currentDateTime.AddDays(_options.RefreshExpirationDays),
                        BID = bid,
                        UserID = userID,
                        ClientID = deviceID,
                    };
                    _authContext.RefreshToken.Add(rt);
                }

                await _authContext.SaveChangesAsync();

                return rt;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        private async Task<RefreshToken> UpdateToken(RefreshToken foundToken)
        {
            foundToken.LastUsedDT = DateTime.UtcNow;
            foundToken.Nonce = Guid.NewGuid();
            _authContext.Update(foundToken);
            await _authContext.SaveChangesAsync();

            return foundToken;
        }

        public async Task<ClaimsIdentity> GetNewIdentity(BusinessUser user, BusinessData bd, Guid? tempUserID = null)
        {
            if (user != null)
            {
                return await GetNewIdentity(user.UserName, user.UserID, tempUserID, bd);
            }

            return null;
        }

        /// <summary>
        /// creates an identity by adding appropriate claims together
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userID"></param>
        /// <param name="bd"></param>
        /// <exception cref="InvalidOperationException">Thrown when no rights and IDs are available for the given business and userID</exception>
        /// <returns></returns>
        private async Task<ClaimsIdentity> GetNewIdentity(string userName, int userID, Guid? tempUserID, BusinessData bd)
        {
            ClaimsIdentity claimsIdent = new ClaimsIdentity(new GenericIdentity(userName, "Token"));
            claimsIdent.AddClaim(new Claim(JwtRegisteredClaimNames.NameId, userName));
            claimsIdent.AddClaim(new Claim(ClaimNameConstants.UserName, userName));
            claimsIdent.AddClaim(new Claim(ClaimNameConstants.BID, bd.BID.ToString()));
            claimsIdent.AddClaim(new Claim(ClaimNameConstants.AID, bd.AssociationID.ToString()));
            claimsIdent.AddClaim(new Claim(ClaimNameConstants.UserID, userID.ToString()));

            var rightsAndID = await GetRightsAndIDs(bd, userID, tempUserID);
            if (rightsAndID == null)
            {
                throw new InvalidOperationException($"{userName} has no rights");
            }
            else
            {
                claimsIdent.AddClaim(new Claim(ClaimNameConstants.AccessType, rightsAndID.accessType));
                claimsIdent.AddClaim(new Claim(ClaimNameConstants.Rights, rightsAndID.rights));
                if (rightsAndID.employeeID.HasValue)
                {
                    claimsIdent.AddClaim(new Claim(ClaimNameConstants.EmployeeID, rightsAndID.employeeID.ToString()));
                }
                else if (rightsAndID.contactID.HasValue)
                {
                    //claimsIdent.AddClaim(new Claim(ClaimNameConstants.ContactID, rightsAndID.contactID.ToString()));
                }

                if (rightsAndID.userlinkID.HasValue)
                {
                    claimsIdent.AddClaim(new Claim(ClaimNameConstants.UserLinkID, rightsAndID.userlinkID.ToString()));
                }
            }

            return claimsIdent;
        }

        /// <summary>
        /// Gets rights and contact/employee ID from the API server via HTTP security endpoint
        /// </summary>
        /// <param name="bd"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        private async Task<BusinessRightsAndIDs> GetRightsAndIDs(BusinessData bd, int userID, Guid? tempUserID)
        {
            if (bd != null)
            {
                //Get Users Rights from API server
                try
                {
                    string tempUserIDQuery;

                    if (tempUserID.HasValue)
                        tempUserIDQuery = $"&tempUserid={tempUserID}";
                    else
                        tempUserIDQuery = "";

                    HttpResponseMessage getRightsResponse = await Http.Client.GetAsync($"{bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.API)?.Server.URL}security?bid={bd.BID}&userid={userID}{tempUserIDQuery}");
                    if (getRightsResponse.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<BusinessRightsAndIDs>(await getRightsResponse.Content.ReadAsStringAsync());
                    }

                    return null;
                }
                catch (Exception ex)
                {
                    await _logger.Error(bd.BID, "Error occurred in GetRightsAndIDs", ex);
                    return null;
                }
            }
            return null;
        }

        private async Task<BusinessUser> GetUser(string username, string password, AuthenticationType authenticationType, Guid? invite = null)
        {
            try
            {
                PasswordHasher<string> pwHasher = new PasswordHasher<string>();
                UserData foundUser;
                if (invite.HasValue)
                {
                    UserInvite inviteUser = await _authContext.UserInvite.FirstOrDefaultAsync(t => t.ID == invite.Value);
                    if (inviteUser == null)
                    {
                        // to do not found
                        return null;
                    }
                    else
                    {
                        if (DateTime.Compare(DateTime.UtcNow, inviteUser.ExpirationDT) < 0)
                        {
                            foundUser = await _authContext.UserData.Include(t => t.RefreshTokens).Include(t => t.ClientData)
                                .Include(t => t.UserCreds).Include(t => t.BusinessLinks)
                                .FirstOrDefaultAsync(t => t.UserName.ToLower() == username.ToLower());

                            if (foundUser != null && inviteUser.IsEmployee)
                            {
                                var existingLink = foundUser.BusinessLinks.Where(t => t.BID == inviteUser.BID).FirstOrDefault();
                                if (existingLink == null)
                                {
                                    var bl = new UserBusinessLink() {
                                        BID = inviteUser.BID,
                                        UserID = foundUser.ID
                                    };
                                    await _authContext.UserBusinessLink.AddAsync(bl);
                                    await _authContext.SaveChangesAsync();

                                    if(!await ConvertToPermanentUser(foundUser, inviteUser))
                                    {
                                        _authContext.UserBusinessLink.Remove(bl);
                                        await _authContext.SaveChangesAsync();
                                    }
                                }

                                var existingCreds = foundUser.UserCreds.Where(t => t.AuthenticationType == authenticationType).FirstOrDefault();
                                if (existingCreds == null)
                                {
                                    var uc = new UserCred()
                                    {
                                        UserID = foundUser.ID,
                                        AuthenticationType = authenticationType,
                                        AuthenticationHash = pwHasher.HashPassword(username, password)
                                    };
                                    await _authContext.UserCred.AddAsync(uc);
                                }
                                else
                                {
                                    existingCreds.AuthenticationHash = pwHasher.HashPassword(username, password);
                                    _authContext.UserCred.Update(existingCreds);
                                }
                                await _authContext.SaveChangesAsync();
                            }
                            else
                            {
                                foundUser = new UserData()
                                {
                                    UserName = inviteUser.EmailAddress,
                                    EmailAddress = inviteUser.EmailAddress,
                                    PhoneNumber = inviteUser.PhoneNumber,
                                    DisplayName = inviteUser.DisplayName
                                };

                                await _authContext.UserData.AddAsync(foundUser);
                                await _authContext.SaveChangesAsync();

                                var bl = new UserBusinessLink()
                                {
                                    BID = inviteUser.BID,
                                    UserID = foundUser.ID
                                };
                                await _authContext.UserBusinessLink.AddAsync(bl);

                                var uc = new UserCred() {
                                    UserID = foundUser.ID,
                                    AuthenticationType = authenticationType,
                                    AuthenticationHash = pwHasher.HashPassword(username, password)
                                };
                                await _authContext.UserCred.AddAsync(uc);
                                await _authContext.SaveChangesAsync();

                                if (!await ConvertToPermanentUser(foundUser, inviteUser))
                                {
                                    _authContext.UserCred.Remove(uc);
                                    _authContext.UserBusinessLink.Remove(bl);
                                    _authContext.UserData.Remove(foundUser);
                                    await _authContext.SaveChangesAsync();
                                }
                            }
                        }
                        else
                        {
                            // to do expired
                            return null;
                        }
                    }

                }
                else
                {
                    foundUser = await _authContext.UserData.Include(t => t.RefreshTokens)
                        .Include(t => t.ClientData).Include(t => t.UserCreds).Include(t => t.BusinessLinks)
                        .FirstOrDefaultAsync(t => username.ToLower() == t.UserName.ToLower());
                }

                if (foundUser == null)
                    return null;

                var cred = foundUser.UserCreds.Where(t => t.AuthenticationType == authenticationType).FirstOrDefault();
                if (cred == null)
                    return null;

                PasswordVerificationResult result = pwHasher.VerifyHashedPassword(foundUser.UserName, cred.AuthenticationHash, password);

                if (result == PasswordVerificationResult.Failed)
                {
                    Debug.WriteLine("Hash failed");
                    return null;
                }

                BusinessUser bu = new BusinessUser()
                {
                    UserID = foundUser.ID,
                    UserName = foundUser.UserName,
                    UserData = foundUser
                };

                return bu;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return null;
            }
        }
        private async Task<BusinessUser> GetUser(string refreshToken, Guid nonce)
        {
            try
            {
                UserData foundUser = await _authContext.UserData.Include(t => t.RefreshTokens).Include(t => t.ClientData).Include("ClientData.Client")
                    .FirstOrDefaultAsync(t => t.RefreshTokens.Any(r => r.Token.ToLower() == refreshToken && r.Nonce == nonce));

                if (foundUser == null)
                    return null;

                BusinessUser bu = new BusinessUser()
                {
                    UserID = foundUser.ID,
                    UserName = foundUser.UserName,
                    UserData = foundUser
                };

                return bu;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Checks if the UserData has any BusinessLinks associated with the BusinessData
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="business">Business</param>
        /// <returns>True if associated links exist</returns>
        private bool HasAssociatedBusinessLinks(UserData user, BusinessData business)
        {
            var isAssociated = false;
            if (user.BusinessLinks != null && user.BusinessLinks.Count() > 0)
            {
                var link = user.BusinessLinks.Where(bl => bl.BID == business.BID).FirstOrDefault();

                if (link != null)
                    isAssociated = true;
            }

            return isAssociated;
        }

        /// <summary>
        /// Calls API to convert an Invited User to a Permanent User
        /// </summary>
        /// <param name="user">User Data</param>
        /// <param name="invite">Invite User</param>
        /// <returns></returns>
        private async Task<bool> ConvertToPermanentUser(UserData user, UserInvite invite)
        {
            try
            {
                var userService = new UserService(_authContext, _options, _logger, null);
                var resp = await userService.ConvertUserToPermanentUser(invite.ID, user.ID);
                if (resp.Item1 == true)
                {
                    user.MustResetPw = false;
                    _authContext.UserData.Update(user);
                    _authContext.UserInvite.Remove(invite);
                    await _authContext.SaveChangesAsync();
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }
    }

    class SystemLanguageType
    {
        public byte ID { get; set; }
        public string Code { get; set; }
        public string GoogleCode { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }

    class OptionResponse
    {
        public OptionValue Value { get; set; }
        public string Message { get; set; }
        public string Success { get; set; }
        public string ErrorMessage { get; set; }
        public bool HasError { get; set; }
    }

    class OptionValue
    {
        public short ID { get; set; }
        public string Value { get; set; }
        public string OptionLevel { get; set; }
    }


}

