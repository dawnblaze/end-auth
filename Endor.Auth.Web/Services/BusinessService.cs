﻿using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Auth.Web.Models;
using Endor.Configuration;
using Endor.Logging.Client;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Extensions.Primitives;

namespace Endor.Auth.Web.Services
{
    public class BusinessService
    {
        #region private instance fields
        /// <summary>
        /// DB authorization context
        /// </summary>
        private readonly AuthContext _authContext;
        /// <summary>
        /// Authorization options
        /// </summary>
        private readonly EndorAuthOptions _options;
        /// <summary>
        /// Remote logger
        /// </summary>
        private readonly RemoteLogger _logger;
        /// <summary>
        /// SMTP Options for sending emails
        /// </summary>
        private readonly IOptions<SMTPOptions> _smtpOptions;
        #endregion

        /// <summary>
        /// User Service used to connect to the DBContext
        /// </summary>
        /// <param name="authContext">DB authorization context</param>
        /// <param name="options">Authorization options</param>
        /// <param name="logger">Remote logger</param>
        public BusinessService(AuthContext authContext, EndorAuthOptions options, RemoteLogger logger, IOptions<SMTPOptions> smtpOptions)
        {
            _authContext = authContext;
            _options = options;
            _logger = logger;
            _smtpOptions = smtpOptions;
        }

        public async Task<GenericResponse> Delete(short ID, bool? TestOnly = false)
        {
            var result = new GenericResponse()
            {
                Success = false,
                Response = "Not Implemented",
            };

            try
            {
                if (!TestOnly.Value)
                    TokenService.RemoveAudience(ID);
                
                object[] myParams = {
                    new Microsoft.Data.SqlClient.SqlParameter("@BID", ID),
                    new Microsoft.Data.SqlClient.SqlParameter("@TestOnly", (bool)TestOnly ? 1:0)
                };
               await _authContext.Database.ExecuteSqlRawAsync("EXEC [Util.Business.Delete] @BID, @TestOnly;", parameters: myParams);
                result.Success = true;
                result.Response = "Sucessfully deleted business data";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Response = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Creates a new Business Object, Location Object, Employee Object, and User Object. 
        /// </summary>
        /// <param name="TemplateBID"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="EmailAddress"></param>
        /// <param name="LocationName"></param>
        /// <param name="NewBusinessID"></param>
        /// <returns></returns>
        public async Task<CreateBusinessResponse> CreateAsync(short TemplateBID, string FirstName, string LastName, string EmailAddress, string LocationName, short? NewBusinessID, IIdentity existingIdentity)
        {
            short NewBID;
            if (NewBusinessID.HasValue)
            {
                //BIDs below 101 are reserved for Business Templates.
                if (NewBusinessID <= 100)
                {
                    return (new CreateBusinessResponse()
                    {
                        BID = NewBusinessID.GetValueOrDefault(-1),
                        Success = false,
                        Response = "BIDs below 101 are reserved for Business Templates."
                    });
                }

                NewBID = NewBusinessID.Value;
            }
            else
            {
                //Generate a new BID
                NewBID = (short)(_authContext.BusinessData
                    .OrderByDescending(b => b.BID).FirstOrDefault().BID + 1);
                if (NewBID <= 100)
                    NewBID = 101;
            }
            
            //Check TemplateBID is existing.
            bool businessExists = await _authContext.Set<BusinessData>().FirstOrDefaultAsync(d => d.BID == TemplateBID) != null;
            if (!businessExists)
            {
                return (new CreateBusinessResponse()
                {
                    BID = TemplateBID,
                    Success = false,
                    Response = "Template business ID not found."
                });
            }

            //Check if provided NewBusinessID is existing.
            bool businessInDatabase = await _authContext.Set<BusinessData>().FirstOrDefaultAsync(d => d.BID == NewBID) != null;
            if (businessInDatabase)
            {
                return (new CreateBusinessResponse()
                {
                    BID = NewBID,
                    Success = false,
                    Response = "Business already found in the AUTH database."
                });
            }

            //Clone Business.Data
            BusinessData templateBusiness = new BusinessData();
            templateBusiness = _authContext.BusinessData.Include(x => x.BusinessCustomUrl).FirstOrDefault(i => i.BID == TemplateBID);
            templateBusiness.BID = NewBID;
            templateBusiness.CreatedDT = DateTime.UtcNow;

            var originalName = templateBusiness.BusinessAppName;
            int currentCloneIdentifier = 2;

            //Ensure record doesn't already exist
            while (await _authContext.Set<BusinessData>().FirstOrDefaultAsync(d => d.BusinessAppName.ToLower() == originalName.ToLower() + "-" + currentCloneIdentifier.ToString()) != null)
            {
                //Record already exists, append clone identifier until it doesn't
                currentCloneIdentifier++;
            }

            templateBusiness.BusinessAppName += "-" + currentCloneIdentifier;

            _authContext.BusinessData.Add(templateBusiness);
            _authContext.SaveChanges();

            try
            {
                var tokenService = new TokenService(_authContext, _options, _logger);
                await tokenService.ReloadAudiences(templateBusiness.BID);
                TokenService.AddAudience(templateBusiness);
            }
            catch (Exception e)
            {
                return (new CreateBusinessResponse()
                {
                    BID = NewBID,
                    Success = false,
                    Response = e.Message
                }); ;
            }

            return (new CreateBusinessResponse()
            {
                BID = NewBID,
                Success = true,
                Response = "Successfully created new business from template."
            });
        }

        public async Task<CreateBusinessResponse> SetActive(short ID, bool active)
        {
            CreateBusinessResponse resp = null;
            try
            {
                var business = await _authContext.BusinessData.FirstOrDefaultAsync(b => b.BID == ID);
                if (business == null)
                {
                    resp = CreateResponse(ID, false, "Business not found");
                    return resp;
                }

                if (active)
                    business.InactiveDT = null;
                else
                    business.InactiveDT = DateTime.UtcNow;

                _authContext.Entry(business).State = EntityState.Modified;
                int affectedRecord = await _authContext.SaveChangesAsync();
                if (affectedRecord > 0)
                {
                    resp = CreateResponse(ID, true, $"{business.BusinessAppName} status updated to {(active ? "active" : "inactive")}.");
                }
                else
                    {
                    resp = CreateResponse(ID, false, "Unable to update active status.");
                }

            }
            catch (Exception ex)
            {
                resp = CreateResponse(ID, false, ex.Message);
            }

            return resp;
        }

        private static CreateBusinessResponse CreateResponse(short ID, bool success, string message)
        {
            return new CreateBusinessResponse()
            {
                Response = message,
                Success = success,
                BID = ID
            };
        }

        /// <summary>
        /// Clones an existing Business to a new Business
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public async Task<CreateBusinessResponse> CloneAsync(short ID)
        {
            return await CreateAsync(ID, null, null, null, null, null, null);
        }
    }
}
