﻿using Endor.Auth.Models.Entities;
using Endor.Auth.Web.Classes;
using Endor.Auth.Web.Models;
using Endor.Configuration;
using Endor.Logging.Client;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Endor.Tenant;

namespace Endor.Auth.Web.Services
{
    public class UserService
    {
        /// <summary>
        /// The password Regex pattern to check against.
        /// Minimum 7 char with 1 upper, 1 lower, and 1 special or numeric
        /// </summary>
        private const string passwordRegexPattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d$@$!%*?&.])[A-Za-z\d$@$!%*?&.]{7,}";
        /// <summary>
        /// The email Regex pattern to check against.
        /// </summary>
        private const string emailRegexPattern = @"^(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)$";

        #region private instance fields
        /// <summary>
        /// DB authorization context
        /// </summary>
        private readonly AuthContext _authContext;
        /// <summary>
        /// Authorization options
        /// </summary>
        private readonly EndorAuthOptions _options;
        /// <summary>
        /// Remote logger
        /// </summary>
        private readonly RemoteLogger _logger;
        /// <summary>
        /// SMTP Options for sending emails
        /// </summary>
        private readonly IOptions<SMTPOptions> _smtpOptions;
        #endregion

        /// <summary>
        /// User Service used to connect to the DBContext
        /// </summary>
        /// <param name="authContext">DB authorization context</param>
        /// <param name="options">Authorization options</param>
        /// <param name="logger">Remote logger</param>
        public UserService(AuthContext authContext, EndorAuthOptions options, RemoteLogger logger, IOptions<SMTPOptions> smtpOptions)
        {
            _authContext = authContext;
            _options = options;
            _logger = logger;
            _smtpOptions = smtpOptions;
        }

        public bool CheckUserName(short bid, string userName)
        {
            UserData existingUser = _authContext.UserData.Include(u => u.BusinessLinks).FirstOrDefault(u => u.EmailAddress.ToLower() == userName.ToLower());

            if (existingUser?.BusinessLinks?.FirstOrDefault(l => l.BID == bid) != null)
                return false;

            return true;
        }

        public int CheckIfExistingUserName(string userName)
        {
            UserData existingUser = _authContext.UserData.FirstOrDefault(u => u.EmailAddress.ToLower() == userName.ToLower());

            if (existingUser != null)
                return existingUser.ID;
            else
                return 0;
        }

        async public Task<(GenerateInviteRequestResponse, string)> InviteUser(GenerateInviteRequest inviteRequest, bool createNewUser = true)
        {
            BusinessData bd = await _authContext.GetBusinessByBIDAsync(inviteRequest.BID);

            if (bd == null)
                return (null, "Business not found");

            if (inviteRequest.SuggestedEmail == null && inviteRequest.Username == null)
            {
                return (null, "Username cannot be null or empty");
            }else if (inviteRequest.SuggestedEmail == null && inviteRequest.Username != null)
            {
                return (null, "Username can only be a valid email address for invite to be send. We don't support PhoneNumber SMS notifications yet.");
            } 
            //If email exists validate the format
            if (inviteRequest.SuggestedEmail != null && !IsEmailValid(inviteRequest.SuggestedEmail))
            {
                return (null, "Username should be a valid email");
            } else if (!CheckUserName(inviteRequest.BID, inviteRequest.SuggestedEmail) && createNewUser)
            {
                return (null, "Username already exists");
            }

            Guid newID = Guid.NewGuid();

            var response = new GenerateInviteRequestResponse()
            {
                ID = newID,
                BID = inviteRequest.BID,
                IsEmployee = inviteRequest.IsEmployee,
                SuggestedEmail = inviteRequest.SuggestedEmail,
                Username = inviteRequest.Username,
                DisplayName = inviteRequest.DisplayName,
                HasImage = inviteRequest.HasImage,
                ImageLocation = inviteRequest.ImageLocation
            };

            var emailService = new SMTPEmailService(_smtpOptions, _logger);

            int existingUserID = CheckIfExistingUserName(inviteRequest.SuggestedEmail);
            if (existingUserID > 0)
            {
                await emailService.SendExistingUserInviteEmail(bd, response);
            }
            else
            {
                if (createNewUser)
                {
                    await emailService.SendUserInviteEmail(bd, response);
                }
                else
                {
                    return (null, "The username specified does not already exist.  You can only add a user that already exists on another system.");
                }
            }

            UserInvite existingInvite = _authContext.UserInvite
                    .FirstOrDefault(i => i.BID == inviteRequest.BID &&
                        i.IsEmployee == inviteRequest.IsEmployee &&
                        i.EmailAddress.ToLower() == inviteRequest.SuggestedEmail.ToLower());

            if (existingInvite != null)
                _authContext.UserInvite.Remove(existingInvite);

            UserInvite invite = new UserInvite()
            {
                ID = newID,
                BID = inviteRequest.BID,
                EmailAddress = inviteRequest.SuggestedEmail,
                DisplayName = inviteRequest.DisplayName,
                IsEmployee = inviteRequest.IsEmployee,
                ExpirationDT = DateTime.UtcNow.AddDays(7)
            };

            _authContext.UserInvite.Add(invite);
            await _authContext.SaveChangesAsync();

            return (response, null);
        }

        async public Task<(bool, string)> RevokeUserInvite(RevokeInviteRequest revokeInviteRequest)
        {
            UserInvite existingInvite = _authContext.UserInvite.FirstOrDefault(i => i.ID == revokeInviteRequest.InviteGUID);
            if (existingInvite != null)
            {
                _authContext.UserInvite.Remove(existingInvite);

                await _authContext.SaveChangesAsync();
            }            

            return (true, null);
        }

        async public Task<(bool, string)> RevokeUserBusinessAccess(UserRevokeAccessRequest revokeAccessRequest)
        {
            var existingUserBusinessLink = _authContext.UserBusinessLink.FirstOrDefault(i => i.BID == revokeAccessRequest.BID
                                                                                        && i.UserID == revokeAccessRequest.UserID);
            if (existingUserBusinessLink != null)
            {
                _authContext.UserBusinessLink.Remove(existingUserBusinessLink);

                await _authContext.SaveChangesAsync();
            }            

            return (true, null);
        }

        async public Task<(bool, string)> ConvertUserToPermanentUser(Guid GUID,int userID)
        {
            //read from UserInvite Table
            var existingInvite = _authContext.UserInvite.FirstOrDefault(i => i.ID == GUID);
            if (existingInvite!=null)
            {
                var bd = await _authContext.GetBusinessByBIDAsync(existingInvite.BID);
                if (bd == null) return (false, "Business could not be found");
                var user = _authContext.UserData.Where(i => i.ID == userID).FirstOrDefault();
                if (user==null) return (false, "Guid for invite could not be found");
                BusinessUser bu = new BusinessUser()
                {
                    UserID = user.ID,
                    UserName = user.UserName,
                    UserData = user
                };
                var tokenService = new TokenService(_authContext, _options, _logger);
                ClaimsIdentity identity = await tokenService.GetNewIdentity(bu, bd, GUID);

                using (var client = new HttpClient())
                {
                    HttpRequestMessage request = 
                        new HttpRequestMessage(HttpMethod.Post, $"{bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.API)?.Server.URL}security/{bd.BID}/makepermanent/{GUID}/to/{user.ID}");
                    request.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(_options.TenantSecret)));

                    HttpResponseMessage response = await client.SendAsync(request);
                    string responseString = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                        return (false, "Unauthorized");

                    if (response.StatusCode != HttpStatusCode.OK)
                        return (false, responseString);
                }
                return (true, "User has been made permanent");
            }
            else
            {
                return (false, "Guid for invite could not be found");
            }
            /*
            var existingUser = _authContext.UserData.FirstOrDefault(i => i.ID == userID);
            if (existingUser != null)
            {
                existingUser.
            }
            await _authContext.SaveChangesAsync();
            return (true, null);
            */
        }

        async public Task<(GeneratePasswordResetResponse, string)> UserResetPasswordRequest(UserResetPasswordRequest request)
        {
            if (request == null)
                return (null, "Must provide BID and UserID.");

            try
            {
                BusinessData bd = null;
                if (request.BID != 0)
                    bd = await _authContext.GetBusinessByBIDAsync(request.BID);
                else
                    bd = await _authContext.GetBusinessByAliasAsync(request.BusinessAlias);

                if (bd == null)
                    return (null, "Business not found");

                UserData user = null;
                if (request.UserID != 0)
                    user = await _authContext.UserData.Include(u => u.BusinessLinks)
                        .FirstOrDefaultAsync(u => u.ID == request.UserID);
                else if (!string.IsNullOrWhiteSpace(request.Username))
                    user = await _authContext.UserData.Include(u => u.BusinessLinks)
                        .FirstOrDefaultAsync(u => u.UserName.ToLower() == request.Username.ToLower());

                if (user == null)
                    return (null, "User not found");

                if (user.BusinessLinks != null && user.BusinessLinks.Count > 0)
                {
                    if (user.BusinessLinks.FirstOrDefault(b => b.BID == bd.BID) == null)
                    {
                        return (null, "User not associated with Business.");
                    }
                }
                else
                {
                    return (null, "User not associated with Business.");
                }

                var resetToken = Guid.NewGuid();
                user.PWResetToken = resetToken;
                user.PWResetTokenExpirationDT = DateTime.UtcNow.AddDays(1);

                _authContext.Update(user);
                await _authContext.SaveChangesAsync();

                var response = new GeneratePasswordResetResponse()
                {
                    ID = resetToken,
                    BID = bd.BID,
                    SuggestedEmail = user.EmailAddress,
                    DisplayName = user.DisplayName,
                    HasImage = false // for now
                };

                var emailService = new SMTPEmailService(_smtpOptions, _logger);
                await emailService.SendResetEmail(bd, response);

                return (response, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Resets a user's password with a newly provided one
        /// </summary>
        /// <param name="resetRequest">A password reset request containing a reset token and new password</param>
        /// <returns>True if successful, false if not</returns>
        public async Task<GenericResponse> ResetPassword(PasswordResetRequest resetRequest)
        {
            var result = new GenericResponse()
            {
                Success = true,
                Response = ""
            };

            try
            {
                result = GetValidTokenAndPassword(resetRequest, out Guid resetToken, out string newPassword);
                if (result.Success)
                {
                    UserData user = await _authContext.UserData.Include(u => u.UserCreds).FirstOrDefaultAsync(u => u.PWResetToken == resetToken);
                    if (user != null)
                    {
                        if (user.PWResetTokenExpirationDT.HasValue && DateTime.Compare(DateTime.UtcNow, user.PWResetTokenExpirationDT.Value) < 0)
                        {
                            user.PWResetToken = null; // clear reset token
                            user.PWResetTokenExpirationDT = null; // clear reset token expiration date
                            user.LastPwChangeDT = DateTime.UtcNow; // it just changed, so update
                            _authContext.Update(user);

                            PasswordHasher<string> pwHasher = new PasswordHasher<string>();
                            var cred = user.UserCreds.Where(c => c.AuthenticationType == AuthenticationType.Password).FirstOrDefault();
                            if (cred != null)
                            {
                                cred.AuthenticationHash = pwHasher.HashPassword(user.UserName, resetRequest.NewPassword);
                                _authContext.UserCred.Update(cred);
                            }
                            else
                            {
                                cred = new UserCred()
                                {
                                    UserID = user.ID,
                                    AuthenticationType = AuthenticationType.Password,
                                    AuthenticationHash = pwHasher.HashPassword(user.UserName, resetRequest.NewPassword)
                                };
                                await _authContext.UserCred.AddAsync(cred);
                            }
                            await _authContext.SaveChangesAsync();

                            result.Response = "Successfully reset password.";
                        }
                        else
                        {
                            result.Response = "Password reset token is no longer valid. Please request a new password reset.";
                            result.Success = false;
                        }
                    }
                    else
                    {
                        // if user is null, then it is an unmatched reset token
                        result.Response = "Password reset token is no longer valid. Please request a new password reset.";
                        result.Success = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Response = ex.Message;

                short? effectiveBID = resetRequest.BID;

                if (!effectiveBID.HasValue && !String.IsNullOrWhiteSpace(resetRequest.BusinessAlias))
                {
                    effectiveBID = (await _authContext.GetBusinessByAliasAsync(resetRequest.BusinessAlias))?.BID;
                }

                if (effectiveBID.HasValue)
                    await _logger.Error(effectiveBID.Value, ex.Message, ex);
            }

            return result;
        }

        #region Private Helpers

        /// <summary>
        /// Determines if the request is valid by checking for a valid password and guid
        /// </summary>
        /// <param name="request">The request to validate</param>
        /// <param name="validGuid">Returned valid guid</param>
        /// <returns></returns>
        private GenericResponse GetValidTokenAndPassword(PasswordResetRequest request, out Guid validGuid, out string validPassword)
        {
            validPassword = "";
            if (request == null)
            {

                validGuid = Guid.Empty;

                return new GenericResponse()
                {
                    Success = false,
                    Response = "Invalid request. Please try again."
                };
            }

            if (!Guid.TryParse(request.ResetToken, out validGuid))
            {
                return new GenericResponse()
                {
                    Success = false,
                    Response = "Invalid reset token."
                };
            }

            if (!IsPasswordValid(request.NewPassword))
            {
                return new GenericResponse()
                {
                    Success = false,
                    Response = "Must provide a valid password: Minimum of 7 characters with 1 uppercase, 1 lowercase, and one number or special character."
                };
            }
            else
            {
                validPassword = request.NewPassword;
            }

            return new GenericResponse()
            {
                Success = true,
                Response = ""
            };
        }

        /// <summary>
        /// Checks if a password matches the validation requirements
        /// </summary>
        /// <param name="password">Password to check against</param>
        /// <returns>True if password is valid, False if it is not</returns>
        private bool IsPasswordValid(string password)
        {
            bool isValid = true;

            isValid = Regex.IsMatch(password, passwordRegexPattern, RegexOptions.Multiline);

            return isValid;
        }

        /// <summary>
        /// Checks if the email address is valid
        /// </summary>
        /// <param name="emailAddress">Email address to validate</param>
        /// <returns></returns>
        private bool IsEmailValid(string emailAddress)
        {
            bool isValid = true;

            isValid = Regex.IsMatch(emailAddress, emailRegexPattern, RegexOptions.IgnoreCase | RegexOptions.Multiline);

            return isValid;
        }

        /// <summary>
        /// Determines if a user is associated with a specific business
        /// </summary>
        /// <param name="bd">Business</param>
        /// <param name="userID">User ID</param>
        /// <returns>True if they are, false if they are not</returns>
        private async Task<bool> IsUserAssocaitedWithBID(BusinessData bd, int userID)
        {
            bool isAssociated = true;
            try
            {
                HttpResponseMessage getRightsResponse = await Http.Client.GetAsync($"{bd.ServerLinks.FirstOrDefault(sl => sl.Server.ServerType == ServerType.API)?.Server.URL}security?bid={bd.BID}&userid={userID}");
                if (!getRightsResponse.IsSuccessStatusCode)
                {
                    isAssociated = false;
                }
            }
            catch
            {
                isAssociated = false;
            }

            return isAssociated;
        }

        #endregion

    }
}
