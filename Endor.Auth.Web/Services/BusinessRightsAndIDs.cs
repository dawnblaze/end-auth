﻿namespace Endor.Auth.Web.Services
{
    public class BusinessRightsAndIDs
    {
        public string accessType;
        public string rights;
        public short? employeeID;
        public int? contactID;
        public short? userlinkID;
    }
}